package ru.vladonishchenko.outerspace.level;

import java.util.ArrayList;
import java.util.List;

import ru.vladonishchenko.outerspace.entity.Entity;
import ru.vladonishchenko.outerspace.entity.Player;
import ru.vladonishchenko.outerspace.screen.Screen;
import ru.vladonishchenko.outerspace.screen.screens.GameScreen;

/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 14:56
 */

public abstract class Level {
    public final List<Entity> entities = new ArrayList<>();
    public Player player;
    public final GameScreen screen;
    protected long tick;

    public Level(GameScreen screen) {
        this.screen = screen;
    }

    public void add(Entity e) {
        entities.add(e);
        e.init(this);
    }

    public abstract void tick();

    public void render(Screen screen) {
        for (int i = entities.size() - 1; i >= 0; i--) {
            Entity e = entities.get(i);
            e.render(screen);
        }
    }
}