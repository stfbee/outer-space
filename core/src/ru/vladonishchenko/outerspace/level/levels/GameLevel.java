package ru.vladonishchenko.outerspace.level.levels;

import java.util.Random;

import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.Stats;
import ru.vladonishchenko.outerspace.entity.Entity;
import ru.vladonishchenko.outerspace.entity.Meteor;
import ru.vladonishchenko.outerspace.entity.Player;
import ru.vladonishchenko.outerspace.entity.UFO;
import ru.vladonishchenko.outerspace.entity.Worm;
import ru.vladonishchenko.outerspace.entity.powerups.Healthy;
import ru.vladonishchenko.outerspace.entity.powerups.Laserous;
import ru.vladonishchenko.outerspace.entity.powerups.Slowly;
import ru.vladonishchenko.outerspace.gui.ChargeBar;
import ru.vladonishchenko.outerspace.gui.HealthBar;
import ru.vladonishchenko.outerspace.level.Level;
import ru.vladonishchenko.outerspace.screen.screens.GameScreen;

/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 14:56
 */

public class GameLevel extends Level {
    final Random random = new Random();
    int wormSpawn;
    private float randomPowerupSpawn;
    private float randomMeteorSpawn;
    private float randomUFOSpawn;

    public GameLevel(GameScreen screen) {
        super(screen);
        player = new Player();
        add(player);

        add(new HealthBar(player));
        add(new ChargeBar(player));

        wormSpawn = 120;
        tick = 0;
        randomMeteorSpawn = (300f + (random.nextFloat() * 800));
        randomPowerupSpawn = (1800 + (random.nextFloat() * 2700));
        randomUFOSpawn = (5000f + (random.nextFloat() * 2500));
    }

    public void tick() {
        tick++;
        randomPowerupSpawn -= OuterSpace.GAMESPEED;
        randomMeteorSpawn -= OuterSpace.GAMESPEED;
        randomUFOSpawn -= OuterSpace.GAMESPEED;
        if (Stats.score > 0 && Stats.score % 500 == 0) {
            if (wormSpawn-- <= 20) wormSpawn = 20;
        }
        if (randomMeteorSpawn <= 0) {
            add(new Meteor());
            randomMeteorSpawn = (800f + (random.nextFloat() * 400));
        }
        if (randomUFOSpawn <= 0) {
            add(new UFO());
            randomUFOSpawn = (5000f + (random.nextFloat() * 2500));
        }
        if (randomPowerupSpawn <= 0) {
            int type = random.nextInt(3) + 1;

            switch (type) {
                case 1:
                    add(new Laserous());
                    break;
                case 2:
                    add(new Healthy());
                    break;
                case 3:
                    add(new Slowly());
                    break;
            }
            randomPowerupSpawn = (2700f + (random.nextFloat() * 1800));
        }
        if (tick % (int) (wormSpawn / OuterSpace.GAMESPEED) == 0) {
            add(new Worm());
        }

        if (player.removed) {
            screen.mayRespawn = true;
        }

        for (int i = 0; i < entities.size(); i++) {
            Entity e = entities.get(i);
            if (!e.removed) {
                e.tick();
            }
            if (e.removed) {
                entities.remove(i--);
            }
        }
    }
}