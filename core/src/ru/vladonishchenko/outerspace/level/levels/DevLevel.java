package ru.vladonishchenko.outerspace.level.levels;

import ru.vladonishchenko.outerspace.entity.Entity;
import ru.vladonishchenko.outerspace.entity.Player;
import ru.vladonishchenko.outerspace.entity.UFO;
import ru.vladonishchenko.outerspace.level.Level;
import ru.vladonishchenko.outerspace.screen.screens.GameScreen;

/**
 * User: Vlad
 * Date: 16.02.14
 * Time: 20:42
 */
public class DevLevel extends Level {

    public DevLevel(GameScreen screen) {
        super(screen);
        player = new Player();
        add(player);
        add(new UFO());
    }
    public void tick() {
        tick++;

        if (player.removed) {
            screen.mayRespawn = true;
        }

        for (int i = 0; i < entities.size(); i++) {
            Entity e = entities.get(i);
            if (!e.removed) {
                e.tick();
            }
            if (e.removed) {
                entities.remove(i--);
            }
        }
    }
}