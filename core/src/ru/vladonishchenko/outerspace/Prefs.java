package ru.vladonishchenko.outerspace;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

import java.util.Locale;

/**
 * User: Vlad
 * Date: 11.02.14
 * Time: 21:55
 */
public class Prefs {
    static Preferences prefs;

    public static void load() {
        prefs = Gdx.app.getPreferences("outerspace");
        if (prefs.getString("score1").equals("")
                || prefs.getString("score2").equals("")
                || prefs.getString("score3").equals("")
                || prefs.getString("score4").equals("")
                || prefs.getString("score5").equals("")) {
            prefs.putBoolean("sound", true);
            prefs.putBoolean("vibro", true);
            prefs.putString("lastName", "Your Name");
            prefs.putString("score1", "Chuck Norris:500000");
            prefs.putString("score2", "Your Mom:100000");
            prefs.putString("score3", "Darth Vader:50000");
            prefs.putString("score4", "Robot Chicken:25000");
            prefs.putString("score5", "Nyan Cat:10000");

            if (Locale.getDefault().getLanguage().equals("ru")) {
                prefs.putString("lang", "ru");
            } else {
                prefs.putString("lang", "en");
            }

            prefs.flush();
        }
    }

    public static void addScore(String name, int score) {
        String[] score1 = prefs.getString("score1").split(":");
        String[] score2 = prefs.getString("score2").split(":");
        String[] score3 = prefs.getString("score3").split(":");
        String[] score4 = prefs.getString("score4").split(":");
        String[] score5 = prefs.getString("score5").split(":");
        String[] names = new String[]{
                score1[0],
                score2[0],
                score3[0],
                score4[0],
                score5[0]
        };
        int[] scores = new int[]{
                Integer.parseInt(score1[1]),
                Integer.parseInt(score2[1]),
                Integer.parseInt(score3[1]),
                Integer.parseInt(score4[1]),
                Integer.parseInt(score5[1])
        };

        for (int i = 0; i < scores.length; i++) {
            if (scores[i] < score) {
                for (int j = scores.length - 1; j > i; j--) {
                    scores[j] = scores[j - 1];
                    names[j] = names[j - 1];
                }
                scores[i] = score;
                names[i] = name;
                break;
            }
        }

        prefs.putString("score1", names[0] + ":" + scores[0]);
        prefs.putString("score2", names[1] + ":" + scores[1]);
        prefs.putString("score3", names[2] + ":" + scores[2]);
        prefs.putString("score4", names[3] + ":" + scores[3]);
        prefs.putString("score5", names[4] + ":" + scores[4]);

        setLastName(name);
        prefs.flush();
    }

    public static boolean isVibro() {
        return prefs.getBoolean("vibro");
    }

    public static boolean isSound() {
        return prefs.getBoolean("sound");
    }

    public static void setVibro(boolean vibro) {
        prefs.putBoolean("vibro", vibro);
        prefs.flush();

    }

    public static void setSound(boolean sound) {
        prefs.putBoolean("sound", sound);
        prefs.flush();
    }

    public static void setLastName(String name) {
        prefs.putString("lastName", name);
        prefs.flush();
    }

    public static String getLastName() {
        return prefs.getString("lastName");
    }

    public static String[][] getScores() {
        String[][] scores;
        String[] score1 = prefs.getString("score1").split(":");
        String[] score2 = prefs.getString("score2").split(":");
        String[] score3 = prefs.getString("score3").split(":");
        String[] score4 = prefs.getString("score4").split(":");
        String[] score5 = prefs.getString("score5").split(":");

        scores = new String[][]{
                score1,
                score2,
                score3,
                score4,
                score5
        };

        return scores;
    }

    public static void resetScores() {
//        prefs.clear();
        prefs.putString("lastName", "Your Name");
        prefs.putString("score1", "Chuck Norris:500000");
        prefs.putString("score2", "Your Mom:100000");
        prefs.putString("score3", "Darth Vader:50000");
        prefs.putString("score4", "Robot Chicken:25000");
        prefs.putString("score5", "Nyan Cat:10000");
        prefs.flush();
        load();
    }

    public static L.l getLang() {
        String lang = prefs.getString("lang");
        if (lang.equals("en")) return L.l.ENGLISH;
        if (lang.equals("ru")) return L.l.RUSSIAN;
        return L.currentLang;
    }

    public static void setLang(L.l lang) {
        String l = "en";
        switch (lang) {
            case ENGLISH:
                l = "en";
                break;
            case RUSSIAN:
                l = "ru";
                break;
        }

        prefs.putString("lang", l);
        prefs.flush();
    }
}
