package ru.vladonishchenko.outerspace.gui;

import com.badlogic.gdx.graphics.g2d.Sprite;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.Input;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.entity.Entity;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: vladik
 * Date: 20.11.13
 * Time: 9:27
 */
public class Warning extends GUI {
    private final Entity owner;
    private int tick;
    private final Sprite sprite = new Sprite(Art.warning2);

    public Warning(Entity owner) {
        float angle = owner.angle + 90f;
        x = (float) (Math.cos(angle * Math.PI / 180) + OuterSpace.GAME_WIDTH / 2 - 14.5);
        y = (float) (Math.sin(angle * Math.PI / 180) + OuterSpace.GAME_HEIGHT / 2 - 32);
//sprite.setOrigin((float) 14.5, 0);
        sprite.setPosition(x, y);
        sprite.setRotation(angle);
        this.owner = owner;
    }

    @Override
    public void render(Screen screen) {
        tick++;
        if (tick >= 360) {
            this.removed = true;
        }

        if (owner.removed) {
            remove();
        }

        //float alpha = (float) Math.abs(Math.sin(tick * 0.05)/2 + 0.2);//values from .2 to .7 //тут владик сильно нахуярился
        float alpha = 1 - tick / (float) 360;
        if (alpha < 0) alpha = 0;

        sprite.setColor(1f, 1f, 1f, alpha);
        sprite.draw(screen.spriteBatch);
    }

    @Override
    public void tick(Input input) {

    }
}