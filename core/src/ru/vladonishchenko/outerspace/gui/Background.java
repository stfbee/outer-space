package ru.vladonishchenko.outerspace.gui;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.entity.Sparkle;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 16.09.2014
 * Time: 23:42
 */
public class Background extends GUI {
    private int tick;
    private float randomSparkleSpawn = (100f + (random.nextFloat() * 950f));
    private final TextureRegion[] stars = new TextureRegion[]{
            Art.stars1,
            Art.stars2,
            Art.stars3
    };

    @Override
    public void render(Screen screen) {

        tick++;
        randomSparkleSpawn -= OuterSpace.GAMESPEED;
        if (randomSparkleSpawn <= 0) {
            if (level != null) level.add(new Sparkle());
            randomSparkleSpawn = (100f + (random.nextFloat() * 950f));
        }
        float alpha1 = (float) Math.abs(Math.sin(tick * .01 * .2));
        float alpha2 = (float) Math.abs(Math.sin(tick * .04 * .2 + 2));
        float alpha3 = (float) Math.abs(Math.sin(tick * .02 * .2 + 5));

        screen.draw(Art.bg, (OuterSpace.GAME_WIDTH - 320) / 2, 0);
        screen.spriteBatch.setColor(1f, 1f, 1f, alpha1);
        screen.draw(stars[0], (OuterSpace.GAME_WIDTH - 320) / 2, 0);
        screen.spriteBatch.setColor(1f, 1f, 1f, alpha2);
        screen.draw(stars[1], (OuterSpace.GAME_WIDTH - 320) / 2, 0);
        screen.spriteBatch.setColor(1f, 1f, 1f, alpha3);
        screen.draw(stars[2], (OuterSpace.GAME_WIDTH - 320) / 2, 0);
        screen.spriteBatch.setColor(1f, 1f, 1f, 1f);
    }
}
