package ru.vladonishchenko.outerspace.gui.buttons;

import ru.vladonishchenko.outerspace.gui.Button;
import ru.vladonishchenko.outerspace.screen.screens.PauseSettingsScreen;

/**
 * User: Vlad
 * Date: 11.02.14
 * Time: 18:17
 */
public abstract class PauseSettingsButtons extends Button {
    public PauseSettingsScreen screen;

    public PauseSettingsButtons(PauseSettingsScreen screen, String string, int position) {
        super(string);
        this.screen = screen;
        this.position = position;

        width = 110;
        height = (int) (textHeight) + 8;

        xButton = -width;
        yButton = 62 + position * 28;

        xText = -width;
        yText = yButton + 4;
    }
}
