package ru.vladonishchenko.outerspace.gui.buttons;

import ru.vladonishchenko.outerspace.gui.Button;
import ru.vladonishchenko.outerspace.screen.screens.PauseScreen;

/**
 * User: Vlad
 * Date: 11.02.14
 * Time: 18:17
 */
public abstract class PauseButtons extends Button {
    public PauseScreen screen;

    public PauseButtons(PauseScreen screen, String string, int position) {
        super(string);
        this.screen = screen;
        this.position = position;

//        width = 74;
        width = 110;
        height = (int) (textHeight) + 8;

//        xButton = position == 0 ? (half - 113) : (position == 1 ? half - 37 : half + 39);
//        yButton = 120;
//
//        xText = (int) (xButton + (width / 2 - textWidth / 2));
//        yText = yButton + 4;
        xButton = -width;
        yButton = 62 + position * 28;

        xText = -width;
        yText = yButton + 4;
    }
}
