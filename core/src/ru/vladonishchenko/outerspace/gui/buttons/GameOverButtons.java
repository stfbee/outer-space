package ru.vladonishchenko.outerspace.gui.buttons;

import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.gui.Button;
import ru.vladonishchenko.outerspace.screen.screens.GameOverScreen;

/**
 * User: Vlad
 * Date: 11.02.14
 * Time: 18:54
 */
public abstract class GameOverButtons extends Button {
    public GameOverScreen screen;

    public GameOverButtons(GameOverScreen screen, String string, int position) {
        super(string);
        this.screen = screen;
        this.position = position;

        width = position == 2 ? 160 : 72;
        height = (int) (textHeight) + 8;

        xButton = position == 2 ? (OuterSpace.GAME_WIDTH / 2 - width / 2) : (position == 1 ? OuterSpace.GAME_WIDTH - 80 : 8);
        yButton = position == 2 ? 62 : OuterSpace.GAME_HEIGHT - 28;

        xText = (int) (xButton + (width / 2 - textWidth / 2));
        yText = yButton + 4;
    }
}