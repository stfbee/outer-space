package ru.vladonishchenko.outerspace.gui.buttons.Settings;

import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.gui.buttons.SettingsButtons;
import ru.vladonishchenko.outerspace.screen.screens.MainMenuScreen;
import ru.vladonishchenko.outerspace.screen.screens.SettingsScreen;

/**
 * User: Vlad
 * Date: 11.02.14
 * Time: 19:37
 */
public class SettingsBack extends SettingsButtons {

    public SettingsBack(SettingsScreen screen) {
        super(screen, L.s("settings.back"), 3);
    }

    @Override
    protected void action() {
        screen.setScreen(new MainMenuScreen());
    }
}
