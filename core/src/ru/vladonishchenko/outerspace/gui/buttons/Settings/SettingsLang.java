package ru.vladonishchenko.outerspace.gui.buttons.Settings;

import com.badlogic.gdx.graphics.g2d.GlyphLayout;

import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.Prefs;
import ru.vladonishchenko.outerspace.gui.buttons.SettingsButtons;
import ru.vladonishchenko.outerspace.screen.screens.SettingsScreen;

/**
 * User: Vlad
 * Date: 11.02.14
 * Time: 19:37
 */
public class SettingsLang extends SettingsButtons {
    L.l lang;
    GlyphLayout glyphLayout = new GlyphLayout();

    public SettingsLang(SettingsScreen screen) {
        super(screen, L.s("settings.lang"), 2);
        lang = Prefs.getLang();
        String l = "EN";
        switch (lang) {
            case ENGLISH:
                l = "EN";
                break;
            case RUSSIAN:
                l = "RU";
                break;
        }
        string = L.s("settings.lang") + ":" + l;
        glyphLayout.setText(font, string);
        textWidth = (int) Math.abs(glyphLayout.width);
        xText = OuterSpace.GAME_WIDTH / 2 - (textWidth - 2) / 2;
    }

    @Override
    protected void action() {
        if (lang == L.l.ENGLISH) lang = L.l.RUSSIAN;
        else lang = L.l.ENGLISH;
        Prefs.setLang(lang);
        String l = "EN";
        switch (lang) {
            case ENGLISH:
                l = "EN";
                break;
            case RUSSIAN:
                l = "RU";
                break;
        }
        string = L.s("settings.lang") + ":" + l;
        glyphLayout.setText(font, string);
        textWidth = (int) Math.abs(glyphLayout.width);
        xText = OuterSpace.GAME_WIDTH / 2 - (textWidth - 2) / 2;
        L.currentLang = lang;
        screen.setScreen(new SettingsScreen());
    }
}
