package ru.vladonishchenko.outerspace.gui.buttons.PauseSettings;

import com.badlogic.gdx.graphics.g2d.GlyphLayout;

import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.Prefs;
import ru.vladonishchenko.outerspace.Sound;
import ru.vladonishchenko.outerspace.gui.buttons.PauseSettingsButtons;
import ru.vladonishchenko.outerspace.screen.screens.PauseSettingsScreen;

/**
 * User: Vlad
 * Date: 11.02.14
 * Time: 19:37
 */
public class PauseSettingsSound extends PauseSettingsButtons {
    boolean soundEnabled;
GlyphLayout glyphLayout = new GlyphLayout();

    public PauseSettingsSound(PauseSettingsScreen screen) {
        super(screen, L.s("settings.sound"), 0);
        soundEnabled = Prefs.isSound();
        string = L.s("settings.sound")+":" + (soundEnabled ? L.s("settings.on") : L.s("settings.off"));
        glyphLayout.setText(font, string);
        textWidth = (int) Math.abs(glyphLayout.width);
        xText = OuterSpace.GAME_WIDTH / 2 - (textWidth - 6) / 2;
    }

    @Override
    protected void action() {
        soundEnabled = !soundEnabled;
        Prefs.setSound(soundEnabled);
        string = L.s("settings.sound")+":" + (soundEnabled ? L.s("settings.on") : L.s("settings.off"));
        glyphLayout.setText(font, string);
        textWidth = (int) Math.abs(glyphLayout.width);
        xText = OuterSpace.GAME_WIDTH / 2 - (textWidth - 6) / 2;
        if(!soundEnabled) Sound.stopMusic();
    }
}
