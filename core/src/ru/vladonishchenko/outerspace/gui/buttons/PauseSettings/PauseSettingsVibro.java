package ru.vladonishchenko.outerspace.gui.buttons.PauseSettings;

import com.badlogic.gdx.graphics.g2d.GlyphLayout;

import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.Prefs;
import ru.vladonishchenko.outerspace.gui.buttons.PauseSettingsButtons;
import ru.vladonishchenko.outerspace.screen.screens.PauseSettingsScreen;

/**
 * User: Vlad
 * Date: 11.02.14
 * Time: 19:37
 */
public class PauseSettingsVibro extends PauseSettingsButtons {
    GlyphLayout glyphLayout = new GlyphLayout(    );
    boolean vibroEnabled;

    public PauseSettingsVibro(PauseSettingsScreen screen) {
        super(screen, L.s("settings.vibro"), 1);
        vibroEnabled = Prefs.isVibro();
        string = L.s("settings.vibro") + ":" + (vibroEnabled ? L.s("settings.on") : L.s("settings.off"));
        glyphLayout.setText(font, string);
        textWidth = (int) Math.abs(glyphLayout.width);
        xText = OuterSpace.GAME_WIDTH / 2 - (textWidth - 2) / 2;
    }

    @Override
    protected void action() {
        vibroEnabled = !vibroEnabled;
        Prefs.setVibro(vibroEnabled);
        Prefs.setVibro(vibroEnabled);
        string = L.s("settings.vibro") + ":" + (vibroEnabled ? L.s("settings.on") : L.s("settings.off"));
        glyphLayout.setText(font, string);
        textWidth = (int) Math.abs(glyphLayout.width);
        xText = OuterSpace.GAME_WIDTH / 2 - (textWidth - 2) / 2;
    }
}
