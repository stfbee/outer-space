package ru.vladonishchenko.outerspace.gui.buttons.PauseSettings;

import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.gui.buttons.PauseSettingsButtons;
import ru.vladonishchenko.outerspace.screen.screens.PauseSettingsScreen;

/**
 * User: Vlad
 * Date: 11.02.14
 * Time: 19:37
 */
public class PauseSettingsBack extends PauseSettingsButtons {

    public PauseSettingsBack(PauseSettingsScreen screen) {
        super(screen, L.s("settings.back"), 2);
    }

    @Override
    protected void action() {
        screen.setScreen(screen.parent);
    }
}
