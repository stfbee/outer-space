package ru.vladonishchenko.outerspace.gui.buttons.Pause;

import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.gui.buttons.PauseButtons;
import ru.vladonishchenko.outerspace.screen.screens.PauseScreen;
import ru.vladonishchenko.outerspace.screen.screens.PauseSettingsScreen;

/**
 * User: Vlad
 * Date: 08.02.14
 * Time: 20:22
 */
public class PauseSettings extends PauseButtons {
    public PauseSettings(PauseScreen screen) {
        super(screen, L.s("pause.settings"), 1);
    }

    @Override
    protected void action() {
        screen.setScreen(new PauseSettingsScreen(screen));
    }
}
