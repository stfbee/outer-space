package ru.vladonishchenko.outerspace.gui.buttons.Pause;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;

import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.gui.buttons.PauseButtons;
import ru.vladonishchenko.outerspace.screen.screens.MainMenuScreen;
import ru.vladonishchenko.outerspace.screen.screens.PauseScreen;

/**
 * User: Vlad
 * Date: 08.02.14
 * Time: 20:22
 */
public class PauseExit extends PauseButtons {
    public PauseExit(PauseScreen screen) {
        super(screen, L.s("pause.exit"),2);
    }

    @Override
    protected void action() {
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
//            OuterSpace.myRequestHandler.showAds(false);
        }
        screen.setScreen(new MainMenuScreen());
    }
}
