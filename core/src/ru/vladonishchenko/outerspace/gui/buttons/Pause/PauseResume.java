package ru.vladonishchenko.outerspace.gui.buttons.Pause;

import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.gui.buttons.PauseButtons;
import ru.vladonishchenko.outerspace.screen.screens.PauseScreen;

/**
 * User: Vlad
 * Date: 08.02.14
 * Time: 20:22
 */
public class PauseResume extends PauseButtons {
    public PauseResume(PauseScreen screen) {
        super(screen, L.s("pause.resume"), 0);
    }

    @Override
    protected void action() {
        screen.setScreen(screen.parent);
    }
}
