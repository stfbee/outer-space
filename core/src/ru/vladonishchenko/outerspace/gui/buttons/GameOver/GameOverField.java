package ru.vladonishchenko.outerspace.gui.buttons.GameOver;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;

import ru.vladonishchenko.outerspace.Input;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.gui.buttons.GameOverButtons;
import ru.vladonishchenko.outerspace.screen.screens.GameOverScreen;

/**
 * User: Vlad
 * Date: 08.02.14
 * Time: 20:22
 */
public class GameOverField extends GameOverButtons {
    Input input;
    GlyphLayout glyphLayout = new GlyphLayout();

    public GameOverField(GameOverScreen screen) {
        super(screen, screen.name, 2);
        font = OuterSpace.font16;
        font.setColor(colorFromHex());

        glyphLayout.setText(font, string);
        textWidth = (int) Math.abs(glyphLayout.width);
        xText = (int) (xButton + (width / 2 - textWidth / 2));
        yText = yButton + 4;
    }

    @Override
    protected void action() {
        Gdx.input.setOnscreenKeyboardVisible(true);
        InputProcessor keyboard = new InputProcessor() {

            @Override
            public boolean keyDown(int keycode) {
                return false;
            }

            @Override
            public boolean keyUp(int keycode) {
                if ((keycode == com.badlogic.gdx.Input.Keys.BACK ||
                        keycode == com.badlogic.gdx.Input.Keys.ENTER ||
                        keycode == com.badlogic.gdx.Input.Keys.ESCAPE) && input != null) {
                    Gdx.input.setInputProcessor(input);
                    Gdx.input.setOnscreenKeyboardVisible(false);
                    screen.name = string;
                } else if (keycode == com.badlogic.gdx.Input.Keys.BACKSPACE) {
                    if (string.length() > 0) {
                        string = string.substring(0, string.length() - 1);
                        glyphLayout.setText(font, string);
                        textWidth = (int) Math.abs(glyphLayout.width);
                        xText = (int) (xButton + (width / 2 - textWidth / 2));
                    }
                }
                return false;
            }

            @Override
            public boolean keyTyped(char character) {
                if (OuterSpace.nedeedLetters.contains(character + "")) {
                    if (string.length() < 16) {
                        string = string + character;
                        glyphLayout.setText(font, string);
                        textWidth = (int) Math.abs(glyphLayout.width);
                        xText = (int) (xButton + (width / 2 - textWidth / 2));
                    }
                }
                return true;
            }

            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer) {
                return false;
            }

            @Override
            public boolean mouseMoved(int screenX, int screenY) {
                return false;
            }

            @Override
            public boolean scrolled(int amount) {
                return false;
            }
        };
        Gdx.input.setInputProcessor(keyboard);
    }

    @Override
    public void tick(Input input) {
        super.tick(input);
        this.input = input;

    }
}
