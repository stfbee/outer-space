package ru.vladonishchenko.outerspace.gui.buttons.GameOver;

import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.Prefs;
import ru.vladonishchenko.outerspace.Stats;
import ru.vladonishchenko.outerspace.gui.buttons.GameOverButtons;
import ru.vladonishchenko.outerspace.screen.screens.GameOverScreen;
import ru.vladonishchenko.outerspace.screen.screens.MainMenuScreen;

/**
 * User: Vlad
 * Date: 08.02.14
 * Time: 20:22
 */
public class GameOverExit extends GameOverButtons {
    public GameOverExit(GameOverScreen screen) {
        super(screen, L.s("gameOver.exit"), 1);
    }

    @Override
    protected void action() {
        OuterSpace.GAMEOVER = false;
        Prefs.addScore(screen.name, Stats.score);
        screen.setScreen(new MainMenuScreen());
    }
}
