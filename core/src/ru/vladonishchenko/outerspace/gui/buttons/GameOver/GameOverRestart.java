package ru.vladonishchenko.outerspace.gui.buttons.GameOver;

import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.Prefs;
import ru.vladonishchenko.outerspace.Stats;
import ru.vladonishchenko.outerspace.gui.buttons.GameOverButtons;
import ru.vladonishchenko.outerspace.screen.screens.GameOverScreen;

/**
 * User: Vlad
 * Date: 08.02.14
 * Time: 20:22
 */
public class GameOverRestart extends GameOverButtons {
    public GameOverRestart(GameOverScreen screen) {
        super(screen, L.s("gameOver.restart"), 0);
    }

    @Override
    protected void action() {
        screen.setScreen(screen.parent);
        Prefs.addScore(screen.name, Stats.score);
        screen.parent.respawnRoom();
    }
}
