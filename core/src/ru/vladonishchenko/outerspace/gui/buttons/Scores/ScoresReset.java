package ru.vladonishchenko.outerspace.gui.buttons.Scores;

import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.Prefs;
import ru.vladonishchenko.outerspace.gui.buttons.ScoresButtons;
import ru.vladonishchenko.outerspace.screen.screens.ScoresScreen;

/**
 * User: Vlad
 * Date: 11.02.14
 * Time: 19:37
 */
public class ScoresReset extends ScoresButtons {

    public ScoresReset(ScoresScreen screen) {
        super(screen, L.s("scores.reset"), 2);
    }

    @Override
    protected void action() {
        Prefs.resetScores();
        screen.setScreen(new ScoresScreen());
    }
}
