package ru.vladonishchenko.outerspace.gui.buttons.Scores;

import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.gui.buttons.ScoresButtons;
import ru.vladonishchenko.outerspace.screen.screens.MainMenuScreen;
import ru.vladonishchenko.outerspace.screen.screens.ScoresScreen;

/**
 * User: Vlad
 * Date: 11.02.14
 * Time: 19:37
 */
public class ScoresBack extends ScoresButtons {

    public ScoresBack(ScoresScreen screen) {
        super(screen, L.s("scores.back"), 3);
    }

    @Override
    protected void action() {
        screen.setScreen(new MainMenuScreen());
    }
}
