package ru.vladonishchenko.outerspace.gui.buttons.MainMenu;

import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.gui.buttons.MainMenuButtons;
import ru.vladonishchenko.outerspace.screen.screens.MainMenuScreen;
import ru.vladonishchenko.outerspace.screen.screens.SettingsScreen;

/**
 * User: Vlad
 * Date: 08.02.14
 * Time: 20:22
 */
public class MainMenuSettings extends MainMenuButtons {
    public MainMenuSettings(MainMenuScreen screen) {
        super(screen, L.s("mainMenu.settings"), 2);
    }

    @Override
    protected void action() {
        screen.setScreen(new SettingsScreen());
    }
}
