package ru.vladonishchenko.outerspace.gui.buttons.MainMenu;

import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.gui.buttons.MainMenuButtons;
import ru.vladonishchenko.outerspace.screen.screens.MainMenuScreen;
import ru.vladonishchenko.outerspace.screen.screens.ScoresScreen;

/**
 * User: Vlad
 * Date: 08.02.14
 * Time: 20:22
 */
public class MainMenuScores extends MainMenuButtons {
    public MainMenuScores(MainMenuScreen screen) {
        super(screen, L.s("mainMenu.scores"), 1);
    }

    @Override
    protected void action() {
        screen.setScreen(new ScoresScreen());
    }
}
