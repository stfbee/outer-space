package ru.vladonishchenko.outerspace.gui.buttons.MainMenu;

import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.gui.buttons.MainMenuButtons;
import ru.vladonishchenko.outerspace.screen.screens.GameScreen;
import ru.vladonishchenko.outerspace.screen.screens.MainMenuScreen;

/**
 * User: Vlad
 * Date: 08.02.14
 * Time: 20:22
 */
public class MainMenuStart extends MainMenuButtons {

    public MainMenuStart(MainMenuScreen screen) {
        super(screen, L.s("mainMenu.start"), 0);
    }

    @Override
    protected void action() {
        screen.setScreen(new GameScreen());
    }
}
