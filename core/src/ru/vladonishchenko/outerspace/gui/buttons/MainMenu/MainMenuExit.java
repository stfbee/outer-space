package ru.vladonishchenko.outerspace.gui.buttons.MainMenu;

import com.badlogic.gdx.Gdx;

import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.gui.buttons.MainMenuButtons;
import ru.vladonishchenko.outerspace.screen.screens.MainMenuScreen;

/**
 * User: Vlad
 * Date: 08.02.14
 * Time: 20:22
 */
public class MainMenuExit extends MainMenuButtons {
    public MainMenuExit(MainMenuScreen screen) {
        super(screen, L.s("mainMenu.exit"), 3);
    }


    @Override
    protected void action() {
        Gdx.app.exit();
    }
}
