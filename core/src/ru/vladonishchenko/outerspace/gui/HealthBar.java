package ru.vladonishchenko.outerspace.gui;

import com.badlogic.gdx.graphics.g2d.Sprite;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.entity.Entity;
import ru.vladonishchenko.outerspace.entity.Player;
import ru.vladonishchenko.outerspace.entity.UFO;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 12.11.13
 * Time: 23:43
 */
public class HealthBar extends GUI {
    private final Entity owner;
    private boolean red = false;
    private int value = 0;
    private double maxValue = 0;
    private int tick = 0;
    private final Sprite overSprite = new Sprite(Art.healthbar_big_over);
    private final TYPE type;

    public HealthBar(Entity owner) {
        if (owner instanceof Player) type = TYPE.PLAYER;
        else if (owner instanceof UFO) type = TYPE.UFO;
        else type = TYPE.WORM;

        this.owner = owner;
        maxValue = owner.health;
    }

    public void tick() {
        tick++;
        switch (type) {
            case PLAYER:
                value = (int) (owner.health * 30);
                break;
            case UFO:
                break;
            case WORM:
                break;

        }
        if (owner.removed) {
            remove();
        }
        if (tick % (int) (20f / OuterSpace.GAMESPEED) == 0) {
            red = value <= 10 && !red;
        }

    }

    @Override
    public void render(Screen screen) {
        Sprite backSprite;
        switch (type) {
            case PLAYER:
                float alpha = ((Player) owner).effectHealthyTime == 0 ? 0 : (float) Math.abs(Math.sin(tick * 0.05));
                overSprite.setColor(1f, 1f, 1f, alpha);
                overSprite.setPosition(4, OuterSpace.GAME_HEIGHT - 24 - 4);
                overSprite.draw(screen.spriteBatch);
                backSprite = new Sprite(Art.healthbar_big);
                backSprite.setPosition(8, OuterSpace.GAME_HEIGHT - 24);
                backSprite.draw(screen.spriteBatch);
                for (int i = 0; i < value; i++) {
                    screen.draw(Art.healthstep_big, 13 + i, OuterSpace.GAME_HEIGHT - 21);
                }
                break;
            case UFO:
                backSprite = new Sprite(Art.healthbar_ufo);
                backSprite.setPosition(owner.x+16-20, owner.y - 4);
                backSprite.draw(screen.spriteBatch);

                for (int i = 1; i <= 10; i++) {
                    if (owner.health >= ((i-1) * maxValue / 10)) {
                        screen.draw(Art.healthbar_ufosteps[0][10 - i], owner.x + 1+16-20, owner.y - 4 + 1);
                    }
                }
                break;
            case WORM:
                backSprite = new Sprite(Art.healthbar_small);
                backSprite.setPosition(owner.x, owner.y + 1);
                backSprite.draw(screen.spriteBatch);

                if (owner.health >= (1 * maxValue / 3))
                    screen.draw(Art.healthbar_worm[0][2], owner.x + 1, owner.y + 1 + 1);
                if (owner.health >= (2 * maxValue / 3))
                    screen.draw(Art.healthbar_worm[0][1], owner.x + 1, owner.y + 1 + 1);
                if (owner.health >= (3 * maxValue / 3))
                    screen.draw(Art.healthbar_worm[0][0], owner.x + 1, owner.y + 1 + 1);
                break;

        }
    }

    enum TYPE {
        PLAYER, UFO, WORM
    }
}
