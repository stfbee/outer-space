package ru.vladonishchenko.outerspace.gui;

import com.badlogic.gdx.math.Polygon;

import ru.vladonishchenko.outerspace.entity.Entity;

/**
 * User: Vlad
 * Date: 08.02.14
 * Time: 19:27
 */
public abstract class GUI extends Entity {
    public Polygon polygon;
}
