package ru.vladonishchenko.outerspace.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.math.Interpolation;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.Input;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 08.02.14
 * Time: 19:39
 */
public abstract class Button extends GUI {
    public final NinePatch patch = Art.button;
    public final float textHeight;
    public float xText, yText;
    public Screen screen;
    public float textWidth;
    public BitmapFont font;
    public String string;
    public float xButton, yButton, height, width;
    public int position;
    private int tick;
    private ANIMATION animation;


    enum ANIMATION {
        IN, IDLE
    }


    protected Button(String string) {
        this.string = string;
        font = OuterSpace.font16;
        font.setColor(colorFromHex());

        GlyphLayout glyphLayout = new GlyphLayout();
        glyphLayout.setText(font, string);
        textWidth = (int) Math.abs(glyphLayout.width);
        textHeight = Math.abs(glyphLayout.height);

        animation = ANIMATION.IN;
    }

    @Override
    public void tick(Input input) {
        tick++;
        runAnimation();
        if (input.buttons[Input.TOUCH] && !input.oldButtons[Input.TOUCH]) {
            int pointx = Input.TOUCHX;
            int pointy = Input.TOUCHY;
            float s = OuterSpace.SCREEN_SCALE;
            if (pointx / s > xButton && pointx / s < xButton + width && pointy / s > yButton && pointy / s < yButton + height) {
                action();
            }
        }
    }

    public void runAnimation() {
        int a = (int) (tick - 5 - 5f * position);
        int w = OuterSpace.GAME_WIDTH;
        float progress = a / 20f;
        progress = Math.min(1f, progress);
        int start = w / 2 + (position % 2 == 0 ? w : -w);
        switch (animation) {
            case IN:
                xButton = Interpolation.pow2Out.apply(start, w / 2, progress) - width / 2;
                xText = Interpolation.pow2Out.apply(start, w / 2, progress) - (textWidth - 6) / 2;
                break;
            case IDLE:
                break;
        }
    }

    @Override
    public void render(Screen screen) {
        patch.draw(screen.spriteBatch, xButton, yButton, width, height);
        screen.drawString(string, xText, yText, font);
    }

    protected abstract void action();

    protected Color colorFromHex() {
        float a = ((long) 0xFFA4A4A4 & 0xFF000000L) >> 24;
        float r = ((long) 0xFFA4A4A4 & 0xFF0000L) >> 16;
        float g = ((long) 0xFFA4A4A4 & 0xFF00L) >> 8;
        float b = ((long) 0xFFA4A4A4 & 0xFFL);

        return new Color(r / 255f, g / 255f, b / 255f, a / 255f);
    }
}
