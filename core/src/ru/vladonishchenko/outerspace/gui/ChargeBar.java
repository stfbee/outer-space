package ru.vladonishchenko.outerspace.gui;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.Input;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.entity.Entity;
import ru.vladonishchenko.outerspace.entity.Player;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 12.11.13
 * Time: 23:43
 */
public class ChargeBar extends GUI {
    public int value = 30;
    public final int valueMax = value;
//    private final Entity owner;
    private final Sprite backSprite = new Sprite(Art.chargebar_big);
    private final Sprite overSprite = new Sprite(Art.chargebar_big_over);
    private final Entity owner;
    private int tick;

    public ChargeBar(Entity owner) {
        this.owner = owner;
        Rectangle bounds = new Rectangle(0, 0, 44, 16);
        polygon = new Polygon(new float[]{0, 0, bounds.width, 0, bounds.width, bounds.height, 0, bounds.height});
    }

    public void tick(Input input) {
    }

    @Override
    public void render(Screen screen) {
        value = (int) (Player.charge * 30);
        if (value > valueMax) value = valueMax;
        if (owner.removed) {remove();}
        backSprite.setPosition(OuterSpace.GAME_WIDTH - 8 - 44, OuterSpace.GAME_HEIGHT - 24);
        backSprite.draw(screen.spriteBatch);

        polygon.setPosition(OuterSpace.GAME_WIDTH - 8 - 44, OuterSpace.GAME_HEIGHT - 24);

        tick++;
        float alpha = value != valueMax ? 0 : (float) Math.abs(Math.sin(tick * 0.05));
        overSprite.setColor(1f, 1f, 1f, alpha);

        overSprite.setPosition(OuterSpace.GAME_WIDTH - 8 - 44 - 4, OuterSpace.GAME_HEIGHT - 24 - 4);
        overSprite.draw(screen.spriteBatch);

        for (int i = 1; i <= value; i++) {
            screen.draw(Art.chargestep_big, OuterSpace.GAME_WIDTH - 13 - i - 4, OuterSpace.GAME_HEIGHT - 21);
        }
    }
}
