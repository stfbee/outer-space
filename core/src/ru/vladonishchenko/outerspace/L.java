package ru.vladonishchenko.outerspace;

import java.util.HashMap;

/**
 * User: Vlad
 * Date: 13.02.14
 * Time: 1:10
 */
public class L {
    public static l currentLang = l.RUSSIAN;

    private final static HashMap<String, String[]> values = new HashMap<>();

    public static void load() {
        values.put("game.title", new String[]{"OUTER SPACE", "OUTER SPACE"});
        values.put("game.pause", new String[]{"PAUSED", "PAUSED"});
        values.put("game.gameOver", new String[]{"GAME OVER", "GAME OVER"});
        values.put("game.demoOver", new String[]{"DEMO OVER", "DEMO OVER"});

        values.put("mainMenu.start", new String[]{"Start", "Старт"});
        values.put("mainMenu.settings", new String[]{"Options", "Настройки"});
        values.put("mainMenu.scores", new String[]{"Scores", "Рекорды"});
        values.put("mainMenu.exit", new String[]{"Exit", "Выход"});
        values.put("mainMenu.touch", new String[]{"TOUCH TO START", "КОСНИТЕСЬ"});

        values.put("scores.back", new String[]{"Back", "Назад"});
        values.put("scores.reset", new String[]{"Reset", "Сброс"});

        values.put("settings.sound", new String[]{"Sound", "Звук"});
        values.put("settings.vibro", new String[]{"Vibro", "Вибро"});
        values.put("settings.on", new String[]{"On", "Вкл"});
        values.put("settings.off", new String[]{"Off", "Выкл"});
        values.put("settings.back", new String[]{"Back", "Назад"});
        values.put("settings.lang", new String[]{"Lang", "Язык"});

        values.put("pause.resume", new String[]{"Resume", "Продолжить"});
        values.put("pause.settings", new String[]{"Options", "Настройки"});
        values.put("pause.exit", new String[]{"Exit", "Выход"});

        values.put("gameOver.exit", new String[]{"Exit", "Выход"});
        values.put("gameOver.restart", new String[]{"Restart", "Еще!"});

        values.put("game.score", new String[]{"Score", "Счёт"});
        values.put("game.healthy", new String[]{"Health+", "Здоровье+"});
        values.put("game.slowly", new String[]{"Matrix style!", "Замедление"});
        values.put("game.laserous", new String[]{"Laser!", "Лазер!"});
        currentLang = Prefs.getLang();
    }

    public static String s(String id) {
        String[] strings = values.get(id);
        if (strings != null) {
            switch (currentLang) {
                case ENGLISH:
                    return strings[0];
                case RUSSIAN:
                    return strings[1];
            }
        }
        return id;
    }

    public enum l {
        ENGLISH, RUSSIAN
    }
}

