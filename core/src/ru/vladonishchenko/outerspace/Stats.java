package ru.vladonishchenko.outerspace;

/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 1:59
 */
public class Stats {
    private static int time;
    public static int score;
    public static int killsWorm;
    public static int killsMeteor;
    public static int flashes;
    public static int shots;
    public static int powerUpLoser;
    public static int powerUps;
    public static int damageMeteor;
    public static int damageWorm;
    public static int killsUFO;

    private Stats() {
        reset();
    }

    public static void reset() {
        shots = 0;
        score = 0;
        flashes = 0;
        killsWorm = 0;
        killsMeteor = 0;
        killsUFO = 0;
        time = 0;
        powerUpLoser = 0;
        powerUps = 0;
        damageMeteor = 0;
        damageWorm = 0;
    }

    public static void tick() {
        if (!OuterSpace.GAMEOVER) {
            time++;
            score = killsWorm * 20 +
                    killsMeteor * 50 +
                    killsUFO * 500 +
                    (int) (time / 60 * OuterSpace.GAMESPEED) - //seconds or smth else
                    powerUpLoser * 100 +
                    powerUps * 45 -
                    damageMeteor * 100 -
                    damageWorm;
        }
    }
}
