package ru.vladonishchenko.outerspace.screen.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.Input;
import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.gui.GUI;
import ru.vladonishchenko.outerspace.gui.buttons.Settings.SettingsBack;
import ru.vladonishchenko.outerspace.gui.buttons.Settings.SettingsLang;
import ru.vladonishchenko.outerspace.gui.buttons.Settings.SettingsSound;
import ru.vladonishchenko.outerspace.gui.buttons.Settings.SettingsVibro;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 11.02.14
 * Time: 15:24
 */
public class SettingsScreen extends Screen {
    @Override
    public void tick(Input input) {
        if (!input.oldButtons[Input.ESCAPE] && input.buttons[Input.ESCAPE]) {
            setScreen(new MainMenuScreen());
        }

        for (GUI gui : guis) {
            gui.tick(input);
        }
    }

    public SettingsScreen() {
        Gdx.input.setCatchBackKey(false);
        guiAdd(new SettingsBack(this));
        guiAdd(new SettingsVibro(this));
        guiAdd(new SettingsSound(this));
        guiAdd(new SettingsLang(this));
    }

    @Override
    public void render() {
        spriteBatch.begin();
        draw(Art.bg, (OuterSpace.GAME_WIDTH - 320) / 2, 0);

//        draw(Art.titleScreen, (SpaceKilla.GAME_WIDTH / 2 - Art.titleScreen.getRegionWidth() / 2), 20);

        String s = L.s("game.title");
        BitmapFont font = OuterSpace.font32;
        font.getData().setScale(2);
        GlyphLayout glyphLayout = new GlyphLayout();
        glyphLayout.setText(font, s);
        int w = (int) glyphLayout.width - 4;
        int half = OuterSpace.GAME_WIDTH / 2;

        font.setColor(new Color(.011f, .011f, .011f, .28f));
        drawString(s, half - w / 2, 24, font);
        font.setColor(Color.WHITE);
        drawString(s, half - w / 2, 20, font);


        String copyright = "2015 STFBEE";
        drawString(copyright, 2, OuterSpace.GAME_HEIGHT - 6 - 2, OuterSpace.font8);
        for (GUI gui : guis) {
            gui.render(this);
        }
        spriteBatch.end();
    }
}
