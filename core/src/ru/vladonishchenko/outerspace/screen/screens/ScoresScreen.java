package ru.vladonishchenko.outerspace.screen.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.Input;
import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.Prefs;
import ru.vladonishchenko.outerspace.gui.GUI;
import ru.vladonishchenko.outerspace.gui.buttons.Scores.ScoresBack;
import ru.vladonishchenko.outerspace.gui.buttons.Scores.ScoresReset;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 11.02.14
 * Time: 15:24
 */
public class ScoresScreen extends Screen {
    final String[][] scores;
    final BitmapFont font = OuterSpace.font8;

    @Override
    public void tick(Input input) {
        if (!input.oldButtons[Input.ESCAPE] && input.buttons[Input.ESCAPE]) {
            setScreen(new MainMenuScreen());
        }

        for (GUI gui : guis) {
            gui.tick(input);
        }
    }

    public ScoresScreen() {
        Gdx.input.setCatchBackKey(false);
        guiAdd(new ScoresBack(this));
        guiAdd(new ScoresReset(this));
        scores = Prefs.getScores();
    }

    @Override
    public void render() {
        spriteBatch.begin();
        draw(Art.bg, (OuterSpace.GAME_WIDTH - 320) / 2, 0);
        GlyphLayout glyphLayout = new GlyphLayout();
        glyphLayout.setText(font, "MMMMMMMMMMMMMMMMMMMMMMMM");
        int w = (int) glyphLayout.width;
        int half = OuterSpace.GAME_WIDTH / 2;

//        draw(Art.titleScreen, (half - Art.titleScreen.getRegionWidth() / 2), 20);
        String s = L.s("game.title");
        BitmapFont font2 = OuterSpace.font32;
        font2.getData().setScale(2);
        glyphLayout.setText(font2, s);
        int w2 = (int) glyphLayout.width - 4;

        font2.setColor(new Color(.011f, .011f, .011f, .28f));
        drawString(s, half - w2 / 2, 24, font2);
        font2.setColor(Color.WHITE);
        drawString(s, half - w2 / 2, 20, font2);

        for (int i = 0; i < 5; i++) {
            drawString(scores[i][0], half - w / 2, 55 + 10 * i, font);
            glyphLayout.setText(font, scores[i][1]);
            drawString(scores[i][1], half + (w / 2 - (int) glyphLayout.width), 55 + 10 * i, font);
        }

        String copyright = "2015 STFBEE";
        drawString(copyright, 2, OuterSpace.GAME_HEIGHT - 6 - 2, font);
        for (GUI gui : guis) {
            gui.render(this);
        }
        spriteBatch.end();
    }
}
