package ru.vladonishchenko.outerspace.screen.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;

import ru.vladonishchenko.outerspace.Input;
import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.gui.GUI;
import ru.vladonishchenko.outerspace.gui.buttons.PauseSettings.PauseSettingsBack;
import ru.vladonishchenko.outerspace.gui.buttons.PauseSettings.PauseSettingsSound;
import ru.vladonishchenko.outerspace.gui.buttons.PauseSettings.PauseSettingsVibro;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 11.02.14
 * Time: 15:24
 */
public class PauseSettingsScreen extends Screen {
    public final PauseScreen parent;

    public PauseSettingsScreen(PauseScreen screen) {
        Gdx.input.setCatchBackKey(false);
        guiAdd(new PauseSettingsBack(this));
        guiAdd(new PauseSettingsVibro(this));
        guiAdd(new PauseSettingsSound(this));
        parent = screen;
    }

    @Override
    public void tick(Input input) {
        if (!input.oldButtons[Input.ESCAPE] && input.buttons[Input.ESCAPE]) {
            setScreen(parent);
        }

        for (GUI gui : guis) {
            gui.tick(input);
        }
    }

    @Override
    public void render() {
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            parent.parent.render();
        }

        spriteBatch.begin();
//        draw(Art.gamemenu, SpaceKilla.GAME_WIDTH / 2 - 56, 35);
//        draw(Art.gamemenu, SpaceKilla.GAME_WIDTH / 2 - Art.gamemenu.getRegionWidth() / 2, 20);
//        draw(Art.titleScreen, (SpaceKilla.GAME_WIDTH / 2 - Art.titleScreen.getRegionWidth() / 2), 20);
        String s = L.s("game.pause");
        BitmapFont font = OuterSpace.font32;
        font.getData().setScale(2);
        GlyphLayout glyphLayout = new GlyphLayout();
        glyphLayout.setText(font, s);
        int w = (int) glyphLayout.width - 4;
        int half = OuterSpace.GAME_WIDTH / 2;

        font.setColor(new Color(.011f, .011f, .011f, .28f));
        drawString(s, half - w / 2, 24, font);
        font.setColor(Color.WHITE);
        drawString(s, half - w / 2, 20, font);

        for (GUI gui : guis) {
            gui.render(this);
        }
        spriteBatch.end();
    }
}
