package ru.vladonishchenko.outerspace.screen.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

import ru.vladonishchenko.outerspace.DemoTimer;
import ru.vladonishchenko.outerspace.Input;
import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.Sound;
import ru.vladonishchenko.outerspace.Stats;
import ru.vladonishchenko.outerspace.gui.Background;
import ru.vladonishchenko.outerspace.level.Level;
import ru.vladonishchenko.outerspace.level.levels.DevLevel;
import ru.vladonishchenko.outerspace.level.levels.GameLevel;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 3:01
 */
public class GameScreen extends Screen {
    private final Background background;
    public boolean mayRespawn = false;
    public Level level;

    public GameScreen() {
        OuterSpace.GAMEOVER = false;

        level = (!OuterSpace.DEBUG_MONSTERS) ? new GameLevel(this) : new DevLevel(this);
//        level = new GameLevel(this);
        Stats.reset();
        Gdx.input.setCatchBackKey(true);
        background = new Background();
        Sound.playGameMusic();
        if (OuterSpace.DEMOMODE) DemoTimer.restartTimer();
    }

    @Override
    public void tick(Input input) {
        if (OuterSpace.running || OuterSpace.GAMEOVER) {
            if (OuterSpace.GAMEOVER) {
                setScreen(new GameOverScreen(this));
            } else {
                Stats.tick();
            }
            if (!input.oldButtons[Input.ESCAPE] && input.buttons[Input.ESCAPE]) {
                setScreen(new PauseScreen(this));
                return;
            }

            if (!input.oldButtons[Input.DEBUG] && input.buttons[Input.DEBUG]) {
                OuterSpace.DEBUG = !OuterSpace.DEBUG;
            }

            if (!level.player.removed) {
                level.player.tick(input);
            }
            level.tick();
            if (OuterSpace.DEMOMODE) {
                DemoTimer.tick();
                if(DemoTimer.isFinished()) OuterSpace.GAMEOVER = true;
            }

            if (Gdx.app.getType() == Application.ApplicationType.Android) {
//                OuterSpace.myRequestHandler.showAds(false);
            }

        } else {
            setScreen(new PauseScreen(this));
        }
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        spriteBatch.begin();
        background.render(this);
        level.render(this);
        drawString(L.s("game.score") + ": " + Stats.score, 2, 2, OuterSpace.font8);
        if (OuterSpace.DEMOMODE) drawStringCenter("Demo time: " + DemoTimer.getTime(), OuterSpace.GAME_WIDTH/2, 2, OuterSpace.fontDemo);
        int offset = 1;
        int mult = 10;
        if (level.player.effectLaserousTime > 0) {
            drawString(L.s("game.laserous"), 2, 2 + offset * mult, OuterSpace.font8);
            offset++;
        }
        if (level.player.effectHealthyTime > 0) {
            drawString(L.s("game.healthy"), 2, 2 + offset * mult, OuterSpace.font8);
            offset++;
        }
        if (level.player.effectSlowlyTime > 0) {
            drawString(L.s("game.slowly"), 2, 2 + offset * mult, OuterSpace.font8);
        }
        spriteBatch.end();
        camera.update();
    }

    public void respawnRoom() {
        OuterSpace.GAMEOVER = false;
        Stats.reset();
        if (OuterSpace.DEMOMODE) DemoTimer.restartTimer();
        this.level = new GameLevel(this);
    }
}
