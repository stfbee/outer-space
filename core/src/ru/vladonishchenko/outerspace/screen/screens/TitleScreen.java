package ru.vladonishchenko.outerspace.screen.screens;

/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 2:29
 */

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.Input;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.screen.Screen;

public class TitleScreen extends Screen {
    private int time = 0;

    public TitleScreen() {
        Gdx.input.setCatchBackKey(false);
        time++;
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        spriteBatch.begin();
        draw(Art.bg, (OuterSpace.GAME_WIDTH - 320) / 2, 0);
        if (time >= 255) {
            setScreen(new MainMenuScreen());
            return;
        } else if (time >= 0) {
            spriteBatch.setColor(1.0f, 1.0f, 1.0f, 255 - time);
            draw(Art.stfbee, (OuterSpace.GAME_WIDTH / 2 - Art.stfbee.getRegionWidth() / 2), 52);
        }
        spriteBatch.end();
    }

    @Override
    public void tick(Input input) {
        time++;
        if (input.buttons[Input.FLASH] && !input.oldButtons[Input.FLASH] || input.buttons[Input.TOUCH] && !input.oldButtons[Input.TOUCH]) {
            if (time > 255) {
//                Sound.startgame.play();
                setScreen(new MainMenuScreen());
                input.releaseAllKeys();
            } else {
                time = 255;
            }
        }
    }
}