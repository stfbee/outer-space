package ru.vladonishchenko.outerspace.screen.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;

import ru.vladonishchenko.outerspace.Input;
import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.Sound;
import ru.vladonishchenko.outerspace.gui.Background;
import ru.vladonishchenko.outerspace.gui.GUI;
import ru.vladonishchenko.outerspace.gui.buttons.MainMenu.MainMenuExit;
import ru.vladonishchenko.outerspace.gui.buttons.MainMenu.MainMenuScores;
import ru.vladonishchenko.outerspace.gui.buttons.MainMenu.MainMenuSettings;
import ru.vladonishchenko.outerspace.gui.buttons.MainMenu.MainMenuStart;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 11.02.14
 * Time: 15:24
 */
public class MainMenuScreen extends Screen {

    Background background;

    public MainMenuScreen() {
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
//            OuterSpace.myRequestHandler.showAds(false);
        }

        Gdx.input.setCatchBackKey(false);
        guiAdd(new MainMenuExit(this));
        guiAdd(new MainMenuStart(this));
        guiAdd(new MainMenuScores(this));
        guiAdd(new MainMenuSettings(this));
//        guiAdd(new Background(this));
        background = new Background();
        Sound.playMenuMusic();
    }

    @Override
    public void tick(Input input) {
        if (!input.oldButtons[Input.ESCAPE] && input.buttons[Input.ESCAPE]) {
            Gdx.app.exit();
        }

        for (GUI gui : guis) {
            gui.tick(input);
        }
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        spriteBatch.begin();
        background.render(this);
        String s = L.s("game.title");
        BitmapFont font = OuterSpace.font32;
        font.getData().setScale(2);
        GlyphLayout glyphLayout = new GlyphLayout();
        glyphLayout.setText(font, s);
        int w = (int) glyphLayout.width - 4;
        int half = OuterSpace.GAME_WIDTH / 2;

        font.setColor(new Color(.011f, .011f, .011f, .28f));
        drawString(s, half - w / 2, 24, font);
        font.setColor(Color.WHITE);
        drawString(s, half - w / 2, 20, font);

        String copyright = "2015 STFBEE";
        drawString(copyright, 2, OuterSpace.GAME_HEIGHT - 6 - 2, OuterSpace.font8);
        for (GUI gui : guis) {
            gui.render(this);
        }
        spriteBatch.end();
    }
}
