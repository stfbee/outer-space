package ru.vladonishchenko.outerspace.screen.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;

import ru.vladonishchenko.outerspace.Input;
import ru.vladonishchenko.outerspace.L;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.entity.Entity;
import ru.vladonishchenko.outerspace.gui.GUI;
import ru.vladonishchenko.outerspace.gui.buttons.Pause.PauseExit;
import ru.vladonishchenko.outerspace.gui.buttons.Pause.PauseResume;
import ru.vladonishchenko.outerspace.gui.buttons.Pause.PauseSettings;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 15:58
 */
public class PauseScreen extends Screen {
    public final GameScreen parent;

    public PauseScreen(GameScreen parent) {
        this.parent = parent;
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
//            if (OuterSpace.ADENABLEAD) OuterSpace.myRequestHandler.showAds(true);
        }

        Gdx.input.setCatchBackKey(true);
        guiAdd(new PauseSettings(this));
        guiAdd(new PauseResume(this));
        guiAdd(new PauseExit(this));
    }

    @Override
    public void render() {
        parent.render();
        spriteBatch.begin();
        for (int i = 0; i < parent.level.entities.size(); i++) {
            Entity e = parent.level.entities.get(i);
            e.render(this);
        }
        for (int i = 0; i < parent.guis.size(); i++) {
            GUI e = parent.guis.get(i);
            e.render(this);
        }
        for (GUI gui : guis) {
            gui.render(this);
        }
        String s = L.s("game.pause");
        BitmapFont font = OuterSpace.font32;
        font.getData().setScale(2);
        GlyphLayout glyphLayout = new GlyphLayout();
        glyphLayout.setText(font, s);
        int w = (int) glyphLayout.width - 4;
        int half = OuterSpace.GAME_WIDTH / 2;

        font.setColor(new Color(.011f, .011f, .011f, .28f));
        drawString(s, half - w / 2, 24, font);
        font.setColor(Color.WHITE);
        drawString(s, half - w / 2, 20, font);

        spriteBatch.end();
    }

    @Override
    public void tick(Input input) {
        if (!input.oldButtons[Input.ESCAPE] && input.buttons[Input.ESCAPE]) {

            setScreen(parent);
            return;
        }

        for (GUI gui : guis) {
            gui.tick(input);
        }
    }
}