package ru.vladonishchenko.outerspace.screen;

/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 2:01
 */

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Matrix4;

import java.util.ArrayList;
import java.util.List;

import ru.vladonishchenko.outerspace.Input;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.gui.GUI;

public abstract class Screen {
    public SpriteBatch spriteBatch;
    public OrthographicCamera camera;
    private OuterSpace spacekilla;
    public final List<GUI> guis = new ArrayList<>();
    public int tick;

    public void removed() {
        spriteBatch.dispose();
    }

    public final void init(OuterSpace spacekilla) {
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            float width = Gdx.graphics.getWidth() / OuterSpace.SCREEN_SCALE;
            float height = Gdx.graphics.getHeight() / OuterSpace.SCREEN_SCALE;

            OuterSpace.GAME_HEIGHT = 180;
            OuterSpace.GAME_WIDTH = (int) (width / height * 180);
            OuterSpace.SCREEN_SCALE = Gdx.graphics.getWidth() / OuterSpace.GAME_WIDTH;
        }

        camera = new OrthographicCamera();

        this.spacekilla = spacekilla;
        Matrix4 projection = new Matrix4();
        projection.setToOrtho(0, OuterSpace.GAME_WIDTH, OuterSpace.GAME_HEIGHT, 0, -1, 1);
        camera.projection.set(projection);

        spriteBatch = new SpriteBatch(100);
        spriteBatch.setProjectionMatrix(camera.projection);
    }

    public void setScreen(Screen screen) {
        spacekilla.setScreen(screen);
    }

    public void draw(TextureRegion region, int x, int y) {
        int width = region.getRegionWidth();
        if (width < 0) width = -width;
        spriteBatch.draw(region, x, y, width, region.getRegionHeight());
    }

    public void draw(TextureRegion region, float x, float y) {
        int width = region.getRegionWidth();
        if (width < 0) width = -width;
        spriteBatch.draw(region, x, y, width, region.getRegionHeight());
    }

    public void drawString(String string, float x, float y, BitmapFont font) {
        font.draw(spriteBatch, string, x, y);
    }

    public void drawStringCenter(String string, int x, int y, BitmapFont font) {
        GlyphLayout glyphLayout = new GlyphLayout();
        glyphLayout.setText(font, string);
        font.draw(spriteBatch, string, x - glyphLayout.width / 2, y);
    }

    public abstract void render();

    public void tick(Input input) {
    }

    public void guiAdd(GUI e) {
        guis.add(e);
    }
}