package ru.vladonishchenko.outerspace;

/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 1:59
 */

import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;

public class Input implements InputProcessor {
    public static final int UP = 0;
    public static final int DOWN = 1;
    private static final int LEFT = 2;
    private static final int RIGHT = 3;
    public static final int SHOOT = 4;
    public static final int DEBUG = 5;
    public static final int FLASH = 6;
    public static final int ESCAPE = 7;
    public static final int TOUCH = 8;
    public static int TOUCHX;
    public static int TOUCHY;
    public final boolean[] buttons = new boolean[64];
    public final boolean[] oldButtons = new boolean[64];



    void set(int key, boolean down) {
        int button = -1;
        if (key == Keys.ESCAPE || key == Keys.MENU || key == Keys.BACK) button = ESCAPE;
        if (key == Keys.DPAD_UP) button = UP;
        if (key == Keys.DPAD_LEFT) button = LEFT;
        if (key == Keys.DPAD_DOWN) button = DOWN;
        if (key == Keys.DPAD_RIGHT) button = RIGHT;

        if (key == Keys.D) button = DEBUG;
        if (key == Keys.SPACE) button = FLASH;
        if (key == Buttons.LEFT) button = SHOOT;
        if (key == Buttons.LEFT||key == Buttons.RIGHT||key == TOUCH) button = TOUCH;

        if (button >= 0) {
            buttons[button] = down;
        }
    }

    void setXY(int x, int y) {
        TOUCHX = x;
        TOUCHY = y;
    }

    public void tick() {
        System.arraycopy(buttons, 0, oldButtons, 0, buttons.length);
    }

    public void releaseAllKeys() {
        for (int i = 0; i < buttons.length; i++) {
            buttons[i] = false;
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        set(keycode, true);
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        set(keycode, false);
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int button) {
        set(TOUCH, true);
        setXY(x, y);
        return false;
    }

    @Override
    public boolean touchUp(int x, int y, int pointer, int button) {
        set(TOUCH, false);
        setXY(x, y);
        return false;
    }

    @Override
    public boolean touchDragged(int x, int y, int pointer) {
        setXY(x, y);
        return false;
    }

    @Override
    public boolean mouseMoved(int x, int y) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }


}