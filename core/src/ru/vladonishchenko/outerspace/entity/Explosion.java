package ru.vladonishchenko.outerspace.entity;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 15:07
 */
public class Explosion extends Entity {
    private int frame;
    private long tick;
    private Sprite sprite;
    private Sprite worm;
    private final Size size;
    private final float originX;
    private final float originY;
    Color color = new Color(.776f, 1, 1, 1);

    public Explosion(float x, float y, Size size, float angle, float originX, float originY, Color color) {
        this.x = x;
        this.y = y;
        this.size = size;
        this.angle = angle;
        this.originX = originX;
        this.originY = originY;
        this.color = color;

        switch (size) {
            case BULLET:
                sprite = new Sprite(Art.explosion[0][3]);
                break;
            case SMALL:
                sprite = new Sprite(Art.explosion[0][2]);
                break;
            case BIG:
                sprite = new Sprite(Art.explosion[0][1]);
                break;
            case WORM:
                sprite = new Sprite(Art.explosion[0][0]);
                worm = new Sprite(Art.explosion[0][3]);
                worm.setColor(color);
                break;
        }
    }

    public Explosion(float x, float y, Size size, float angle, float originX, float originY) {
        this.x = x;
        this.y = y;
        this.size = size;
        this.angle = angle;
        this.originX = originX;
        this.originY = originY;

        switch (size) {
            case BULLET:
                sprite = new Sprite(Art.explosion[0][3]);
                break;
            case SMALL:
                sprite = new Sprite(Art.explosion[0][2]);
                break;
            case BIG:
                sprite = new Sprite(Art.explosion[0][1]);
                break;
            case WORM:
                sprite = new Sprite(Art.explosion[0][0]);
                worm = new Sprite(Art.explosion[5][0]);
                worm.setColor(color);
                break;
        }
//        float volume = (float) (Math.pow(position*position/(OuterSpace.GAME_WIDTH / 2), 2));
//        Sound.playRandom(Sound.explosion, volume);
    }

    @Override
    public void tick() {
        tick++;
        if (tick % (int) (5f / OuterSpace.GAMESPEED) == 0) {
            frame++;
        }
        switch (size) {
            case BULLET:
                if (frame == 4) {
                    remove();
                    return;
                }
                sprite.setRegion(Art.explosion[frame][3]);
                break;
            case SMALL:
                if (frame == 6) {
                    remove();
                    return;
                }
                sprite.setRegion(Art.explosion[frame][2]);
                break;
            case BIG:
                if (frame == 6) {
                    remove();
                    return;
                }
                sprite.setRegion(Art.explosion[frame][1]);
                break;
            case WORM:
                if (frame == 5) {
                    remove();
                    return;
                }
                sprite.setRegion(Art.explosion[frame][0]);
                if (frame < 3) {
                    worm.setRegion(Art.explosion[frame + 5][0]);
                } else {
                    worm.setRegion(Art.explosion[frame + 3][1]);
                }
                break;
        }

        sprite.setPosition(x - sprite.getOriginX() + originX, y - sprite.getOriginY() + originY);
        if (size == Size.WORM) {
            worm.setPosition(x - sprite.getOriginX() + originX, y - sprite.getOriginY() + originY);
            worm.setRotation(angle);
            sprite.setRotation(angle);
        }
    }

    @Override
    public void render(Screen g) {
        sprite.draw(g.spriteBatch);
        if (size == Size.WORM) worm.draw(g.spriteBatch);
    }

    public enum Size {
        SMALL, BIG, WORM, BULLET
    }
}