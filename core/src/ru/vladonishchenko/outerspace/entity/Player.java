package ru.vladonishchenko.outerspace.entity;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;

import org.lwjgl.input.Mouse;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.Input;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.Prefs;
import ru.vladonishchenko.outerspace.Sound;
import ru.vladonishchenko.outerspace.Stats;
import ru.vladonishchenko.outerspace.entity.bullets.Bomb;
import ru.vladonishchenko.outerspace.entity.bullets.Flash;
import ru.vladonishchenko.outerspace.entity.bullets.Laser;
import ru.vladonishchenko.outerspace.entity.powerups.Healthy;
import ru.vladonishchenko.outerspace.entity.powerups.Laserous;
import ru.vladonishchenko.outerspace.entity.powerups.Slowly;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 15:03
 */
public class Player extends Entity {
    public static float angle;
    public static float charge;
    public final Polygon polygon;
    private int bulletsType;
    private int shootTime;
    public float effectLaserousTime;
    public float effectHealthyTime;
    public float effectSlowlyTime;
    private Sprite sprite;
    private int tick;

    public Player() {
        health = 1;
        Rectangle bounds = new Rectangle(0, 0, 14, 18);
        polygon = new Polygon(new float[]{0, 0, bounds.width, 0, bounds.width, bounds.height, 0, bounds.height});

        angle = 0;
        sprite = new Sprite(Art.player);

        bulletsType = 0;
        shootTime = 10;

        charge = 1;

        sprite.setPosition(OuterSpace.GAME_WIDTH / 2 - 7, OuterSpace.GAME_HEIGHT / 2 - 9);
        sprite.setOrigin(7, 9);
        sprite.setRotation(angle);
        sprite.setColor(1f, 1f, 1f, 0f);
        polygon.setPosition(OuterSpace.GAME_WIDTH / 2 - 7, OuterSpace.GAME_HEIGHT / 2 - 9);
        polygon.setOrigin(7, 9);
        polygon.setRotation(angle);
    }

    @Override
    public void render(Screen g) {
        sprite.draw(g.spriteBatch);
    }

    public void tick(Input input) {
        tick++;
        int mx = getX(), my = getY();
        float previousAngle = angle;
        angle = (float) ((Math.atan2(my, mx) * 180.0d / Math.PI) + 90.0f);

        if (previousAngle > angle) {
            sprite = new Sprite(Art.player_left);
        } else if (previousAngle < angle) {
            sprite = new Sprite(Art.player_right);
        } else {
            sprite = new Sprite(Art.player);
        }

        if (charge > 1) charge = 1;

        sprite.setPosition(OuterSpace.GAME_WIDTH / 2 - 7, OuterSpace.GAME_HEIGHT / 2 - 9);
        sprite.setRotation(angle);
        polygon.setPosition(OuterSpace.GAME_WIDTH / 2 - 7, OuterSpace.GAME_HEIGHT / 2 - 9);
        polygon.setRotation(angle);

        boolean shooting = false;

        if (input.buttons[Input.TOUCH] && !input.oldButtons[Input.TOUCH]) {
            int s = (int) OuterSpace.SCREEN_SCALE;
            int w = OuterSpace.GAME_WIDTH;
            int h = OuterSpace.GAME_HEIGHT;

            if (Gdx.app.getType() == Application.ApplicationType.Android) {
                int x = Input.TOUCHX;
                int y = Input.TOUCHY;

                if (x >= (w - 52) * s && y >= (h - 24) * s) {
                    shoot(2);
                } else {
                    shoot(bulletsType);
                }

            } else {
                int x = Mouse.getX();
                int y = Mouse.getY();

                if (x >= (w - 52) * s && y <= 24 * s) {
                    shoot(2);
                } else {
                    shoot(bulletsType);
                }
            }
        }

        shootTime++;
        if (input.buttons[Input.TOUCH] && shootTime > 0) {
            shooting = bulletsType == 1 || shootTime % (int) (10f / OuterSpace.GAMESPEED) == 0;//WARNING!
        } else {
            shootTime = 0;
        }
        if (input.buttons[Input.FLASH] && !input.oldButtons[Input.FLASH]) {
            shoot(2);
        } else if (shooting) {
            shoot(bulletsType);
        }

        if (tick % (int) (500f / OuterSpace.GAMESPEED) == 0) {
            health += .1;
            if (health > 1) health = 1;
        }

        if (effectHealthyTime > 0) {
            effectHealthyTime -= OuterSpace.GAMESPEED;
            health = 1;
        } else {
            effectHealthyTime = 0;
        }
        if (effectLaserousTime > 0) {
            effectLaserousTime -= OuterSpace.GAMESPEED;
            bulletsType = 1;
        } else {
            bulletsType = 0;
            effectLaserousTime = 0;
        }
        if (effectSlowlyTime > 0) {
            effectSlowlyTime -= OuterSpace.GAMESPEED;
            OuterSpace.GAMESPEED = .5f;
        } else {
            OuterSpace.GAMESPEED = 1f;
            effectSlowlyTime = 0;
        }

        if (level != null) {
            for (Entity e : level.entities) {
                if (e instanceof Worm) {
                    if (Intersector.overlapConvexPolygons(((Worm) e).polygon, this.polygon)) {
                        if (((Worm) e).damage != 0) {
                            health -= ((Worm) e).damage;
                            if (!((Worm) e).stopped) Sound.playRandom(Sound.hit);
                            ((Worm) e).stopped = true;
                            if (health <= 0) die();
                            if (Prefs.isVibro()) Gdx.input.vibrate(200);
                            Stats.damageWorm++;
                        }
                    }
                } else if (e instanceof Meteor) {
                    if (Intersector.overlapConvexPolygons(((Meteor) e).polygon, this.polygon)) {
                        health -= ((Meteor) e).damage;
                        ((Meteor) e).health = 0;
                        Sound.playRandom(Sound.hit);
                        if (health <= 0) die();
                        if (Prefs.isVibro()) Gdx.input.vibrate(200);
                        Stats.damageMeteor++;
                    }
                } else if (e instanceof PowerUp) {
                    if (Intersector.overlapConvexPolygons(((PowerUp) e).polygon, this.polygon)) {
                        e.remove();
                        Sound.playRandom(Sound.powerup);
                        if (Prefs.isVibro()) Gdx.input.vibrate(200);
                        Stats.powerUps++;

                        if (e instanceof Laserous) {
                            effectLaserousTime = 60 * 10;
                        } else if (e instanceof Healthy) {
                            effectHealthyTime = 60 * 20;
                        } else if (e instanceof Slowly) {
                            effectSlowlyTime = 60 * 7;
                        }
                    }
                }
            }
        }
    }

    private void shoot(int bulletsType) {
        if (bulletsType == 0) {
            level.add(new Bomb());
            Sound.playRandom(Sound.shot);
            Stats.shots++;
            if (Prefs.isVibro()) Gdx.input.vibrate(25);
        } else if (bulletsType == 1) {
            level.add(new Laser());
            Sound.playRandom(Sound.laser, .75f);
            Stats.shots++;
            if (Prefs.isVibro()) Gdx.input.vibrate(50);
        } else if (bulletsType == 2) {
            if (Player.charge == 1) {
                level.add(new Flash());
                Player.charge = 0;
                Sound.playRandom(Sound.flash);
                Stats.flashes++;
                if (Prefs.isVibro()) Gdx.input.vibrate(50);
            }
        }

    }

    void die() {
        if (removed) return;
        remove();

        if (Prefs.isVibro()) Gdx.input.vibrate(700);
        OuterSpace.GAMEOVER = true;
    }

    int getX() {
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            return (int) (-OuterSpace.GAME_WIDTH / 2 + Input.TOUCHX / (float) Gdx.graphics.getWidth() * OuterSpace.GAME_WIDTH);
        } else {
            double k = Gdx.graphics.getWidth() / OuterSpace.GAME_WIDTH;
            return (int) (-OuterSpace.GAME_WIDTH / 2 + Mouse.getX() / k);
        }
    }

    int getY() {
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            return (int) (-OuterSpace.GAME_HEIGHT / 2 + Input.TOUCHY / (float) Gdx.graphics.getHeight() * OuterSpace.GAME_HEIGHT);
        } else {
            double k = Gdx.graphics.getHeight() / OuterSpace.GAME_HEIGHT;
            return (int) -(-OuterSpace.GAME_HEIGHT / 2 + Mouse.getY() / k);
        }
    }
}