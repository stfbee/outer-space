package ru.vladonishchenko.outerspace.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.Prefs;
import ru.vladonishchenko.outerspace.Sound;
import ru.vladonishchenko.outerspace.Stats;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 07.02.14
 * Time: 23:51
 */
public abstract class PowerUp extends Entity {
    public final Polygon polygon;
    public Sprite sprite = new Sprite(Art.powerups[0][0]);
    public Sprite sprite1 = new Sprite(Art.powerups[0][1]);
    public Sprite sprite2 = new Sprite(Art.powerups[0][1]);
    int rotation = 0;
    float dx, dy;
    private int tick;
    private int frame;

    protected PowerUp() {
        Rectangle bounds = new Rectangle(0, 0, 6, 9);
        polygon = new Polygon(new float[]{0, 0, bounds.width, 0, bounds.width, bounds.height, 0, bounds.height});

        health = .1;
        angle = random.nextInt(360);
        float dangle;
        do {
            dangle = random.nextInt(60);
        } while (dangle < 45);
        dangle -= 30;

        position = (float) Math.sqrt(OuterSpace.GAME_HEIGHT * OuterSpace.GAME_HEIGHT + OuterSpace.GAME_WIDTH * OuterSpace.GAME_WIDTH) / 2;
        float startX = (float) (Math.cos((angle + dangle) * Math.PI / 180) * position + OuterSpace.GAME_WIDTH / 2 - 3);
        float startY = (float) (Math.sin((angle + dangle) * Math.PI / 180) * position + OuterSpace.GAME_HEIGHT / 2 - 4.5);
        x = (float) (Math.cos(angle * Math.PI / 180) * position + OuterSpace.GAME_WIDTH / 2 - 3);
        y = (float) (Math.sin(angle * Math.PI / 180) * position + OuterSpace.GAME_HEIGHT / 2 - 4.5);
        dx = startX - x;
        dy = startY - y;

        sprite.setPosition(x, y);
        sprite.setRotation(angle + 90f + rotation);
        sprite.setColor(1f, 1f, 1f, 0f);
        polygon.setPosition(x, y);
        polygon.setOrigin(14, 6);
        polygon.setRotation(angle + 90f + rotation);
    }

    @Override
    public void tick() {
        position -= (.55f * OuterSpace.GAMESPEED);
        rotation = rotation >= 360 ? 0 : rotation + 1;

        x = (float) (Math.cos(angle * Math.PI / 180) * position + OuterSpace.GAME_WIDTH / 2 - 3 + dx);
        y = (float) (Math.sin(angle * Math.PI / 180) * position + OuterSpace.GAME_HEIGHT / 2 - 4.5 + dy);

        tick++;
        float frameRate = 30f;
        if (tick % (int) (frameRate / OuterSpace.GAMESPEED) == 0) {
            frame++;
            frame = frame % 2;
        }
        sprite = frame == 1 ? sprite1 : sprite2;

        sprite.setPosition(x, y);
        sprite.setRotation(angle + 90f + rotation);
        sprite.setColor(1f, 1f, 1f, 1f);
        polygon.setPosition(x, y);
        polygon.setOrigin(3, (float) 4.5);
        polygon.setRotation(angle + 90f + rotation);

        if (position < 0) outOfBounds();
        if (health <= 0) {
            Sound.playRandom(Sound.hit);
            level.add(new Explosion(x + 3 - OuterSpace.GAME_WIDTH / 2, y + 4 - OuterSpace.GAME_HEIGHT / 2, Explosion.Size.SMALL, sprite.getRotation(), sprite.getOriginX(), sprite.getOriginY()));
            Stats.powerUpLoser++;
            if (Prefs.isVibro()) Gdx.input.vibrate(300);
            this.remove();
        }
    }

    @Override
    public void outOfBounds() {
        if (-position > radius) {
            health = 0;
        }
    }

    @Override
    public void render(Screen screen) {
        sprite.draw(screen.spriteBatch);
    }
}
