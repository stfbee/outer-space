package ru.vladonishchenko.outerspace.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import ru.vladonishchenko.outerspace.Prefs;
import ru.vladonishchenko.outerspace.Sound;
import ru.vladonishchenko.outerspace.Stats;
import ru.vladonishchenko.outerspace.entity.bullets.Bomb;
import ru.vladonishchenko.outerspace.entity.bullets.Laser;
import ru.vladonishchenko.outerspace.entity.bullets.UFOBomb;
import ru.vladonishchenko.outerspace.entity.powerups.Healthy;
import ru.vladonishchenko.outerspace.entity.powerups.Laserous;
import ru.vladonishchenko.outerspace.entity.powerups.Slowly;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 15:03
 */
public abstract class Bullet extends Entity {
    private final Rectangle bounds = new Rectangle(0, 0, 2, 4);
    protected Polygon polygon = new Polygon(new float[]{0, 0, bounds.width, 0, bounds.width, bounds.height, 0, bounds.height});
    protected float damage;
    protected Sprite sprite;


    protected Bullet() {
        angle = Player.angle - 90;
        position = 0;
    }

    public void tick() {
        sprite.setPosition(x, y);
        sprite.setRotation(angle + 90f);
        sprite.setOrigin(1, 2);
        polygon.setPosition(x, y);
        polygon.setOrigin(1, 2);
        polygon.setRotation(angle + 90f);

        outOfBounds();
        for (Entity e : level.entities) {
            if (e instanceof Worm) {
                if (Intersector.overlapConvexPolygons(((Worm) e).polygon, this.polygon)) {
                    Player.charge += .01;
                    Sound.playRandom(Sound.hit);
                    if (Prefs.isVibro()) Gdx.input.vibrate(100);
                    if (this instanceof Bomb) {
                        e.health -= this.damage;
                        this.remove();
                    } else if (this instanceof Laser) {
                        e.health = 0;
                    }
                    break;
                }
            } else if (e instanceof UFO) {
                if (overlaps(((UFO) e).polygon, this.polygon)) {
                    if (!(this instanceof UFOBomb)) {
                        Player.charge += .01;
                        Sound.playRandom(Sound.hit);
                        if (Prefs.isVibro()) Gdx.input.vibrate(100);
                        if (this instanceof Bomb) {
                            e.health -= this.damage;
                            this.remove();
                        } else if (this instanceof Laser) {
                            e.health -= 5f;
                        }
                    }
                    break;
                }
            } else if (e instanceof Meteor) {
                if (((Meteor) e).enabled) {
                    if (Intersector.overlapConvexPolygons(((Meteor) e).polygon, this.polygon)) {

                        Sound.playRandom(Sound.hit);
                        if (Prefs.isVibro()) Gdx.input.vibrate(250);
                        if (this instanceof Bomb) {
                            e.health -= this.damage;
                            this.remove();
                        } else if (this instanceof Laser) {
                            e.health = 0;
                        }
                        break;
                    }
                }
            } else if (e instanceof PowerUp) {
                if (Intersector.overlapConvexPolygons(((PowerUp) e).polygon, this.polygon)) {
//                    Sound.playRandom(Sound.hit);
//                    if (Prefs.isVibro()) Gdx.input.vibrate(300);//                    if (this instanceof Bomb) {
//                        e.health -= this.damage;
//                        this.remove();
//                    } else if (this instanceof Laser) {
//                        e.health = 0;
//
//                    }
//                    break;

                    Sound.playRandom(Sound.powerup);
                    if (Prefs.isVibro()) Gdx.input.vibrate(200);
                    Stats.powerUps++;

                    if (e instanceof Laserous) {
                        level.player.effectLaserousTime = 60 * 10;
                    } else if (e instanceof Healthy) {
                        level.player.effectHealthyTime = 60 * 20;
                    } else if (e instanceof Slowly) {
                        level.player.effectSlowlyTime = 60 * 7;
                    }
                    e.remove();
                    break;
                }

            } else if (e instanceof Player) {
                if (this instanceof UFOBomb) {
                    if (Intersector.overlapConvexPolygons(((Player) e).polygon, this.polygon)) {
                        Sound.playRandom(Sound.hit);
                        if (Prefs.isVibro()) Gdx.input.vibrate(150);
                        e.health -= this.damage;
                        this.remove();
                        break;
                    }
                }
            } else if (e instanceof Bullet) {
                if (this != e && !(e instanceof Laser)) {
                    if (Intersector.overlapConvexPolygons(((Bullet) e).polygon, this.polygon)) {
                        Sound.playRandom(Sound.hit);
                        if (Prefs.isVibro()) Gdx.input.vibrate(150);
                        level.add(new Explosion(e.x, e.y, Explosion.Size.BULLET, sprite.getRotation(), sprite.getOriginX(), sprite.getOriginY()));
                        e.remove();
                        this.remove();
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void render(Screen g) {
        sprite.draw(g.spriteBatch);
    }

    public static boolean overlaps(Circle circle, Polygon polygon) {
        float[] vertices = polygon.getTransformedVertices();
        Vector2 center = new Vector2(circle.x, circle.y);
        float squareRadius = circle.radius * circle.radius;
        for (int i = 0; i < vertices.length; i += 2) {
            if (i == 0) {
                if (Intersector.intersectSegmentCircle(new Vector2(vertices[vertices.length - 2], vertices[vertices.length - 1]), new Vector2(vertices[i], vertices[i + 1]), center, squareRadius))
                    return true;
            } else {
                if (Intersector.intersectSegmentCircle(new Vector2(vertices[i - 2], vertices[i - 1]), new Vector2(vertices[i], vertices[i + 1]), center, squareRadius))
                    return true;
            }
        }
        return false;
    }
}
