package ru.vladonishchenko.outerspace.entity;

import java.util.Random;

import ru.vladonishchenko.outerspace.Input;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.level.Level;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 15:00
 */
public abstract class Entity {
    protected static final Random random = new Random();
    public static Level level;
    public float x;
    public float y;
    public boolean removed = false;
    public double health;
    public float angle;
    public float position;
    protected final float radius = (float) Math.sqrt(OuterSpace.GAME_HEIGHT * OuterSpace.GAME_HEIGHT + OuterSpace.GAME_WIDTH * OuterSpace.GAME_WIDTH);

    public void init(Level level) {
        Entity.level = level;
    }

    public void remove() {
        removed = true;
    }

    public void tick() {
    }

    public void tick(Input input) {
    }

    public abstract void render(Screen screen);

    public void outOfBounds() {
        if (position > radius) {
            remove();
        }
    }
}