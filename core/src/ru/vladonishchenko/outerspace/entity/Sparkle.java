package ru.vladonishchenko.outerspace.entity;

import com.badlogic.gdx.graphics.g2d.Sprite;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 20.09.2014
 * Time: 10:55
 */
public class Sparkle extends Entity {
    private int frame;
    private long tick;
    private Sprite sprite;

    public Sparkle() {
        x = random.nextInt(OuterSpace.GAME_WIDTH);
        y = random.nextInt(OuterSpace.GAME_HEIGHT);
        sprite = new Sprite(Art.sparkle[0][0]);
        sprite.setPosition(x, y);
    }

    @Override
    public void tick() {
        tick++;
        if (tick % (int) (3.5f / OuterSpace.GAMESPEED) == 0) {
            if (frame++ >= 8) remove();
        }
        sprite.setRegion(Art.sparkle[frame][0]);
    }

    @Override
    public void render(Screen g) {
        sprite.draw(g.spriteBatch);
    }
}
