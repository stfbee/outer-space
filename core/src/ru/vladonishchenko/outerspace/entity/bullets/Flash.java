package ru.vladonishchenko.outerspace.entity.bullets;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.Sound;
import ru.vladonishchenko.outerspace.Stats;
import ru.vladonishchenko.outerspace.entity.Bullet;
import ru.vladonishchenko.outerspace.entity.Entity;
import ru.vladonishchenko.outerspace.entity.Meteor;
import ru.vladonishchenko.outerspace.entity.PowerUp;
import ru.vladonishchenko.outerspace.entity.UFO;
import ru.vladonishchenko.outerspace.entity.Worm;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: Vlad
 * Date: 14.01.14
 * Time: 7:32
 */
public class Flash extends Entity {
    protected final double damage;
    Circle circle = new Circle(OuterSpace.GAME_WIDTH / 2, OuterSpace.GAME_HEIGHT / 2, 1);
    private final Sprite sprite = new Sprite(Art.flashPixel);
    private boolean UFODamaged = false;

    public Flash() {
        damage = .75;
        angle = random.nextInt(360);
    }

    @Override
    public void tick() {
        position += 2;
        circle = new Circle(OuterSpace.GAME_WIDTH / 2, OuterSpace.GAME_HEIGHT / 2, position);
        outOfBounds();

        for (Entity e : level.entities) {
            if (e instanceof Worm) {
                if (Bullet.overlaps(circle, ((Worm) e).polygon)) {
                    e.health -= this.damage;
                    Sound.playRandom(Sound.hit);

                    break;
                }
            }else if (e instanceof UFO) {
                if(!UFODamaged)if (Intersector.overlaps(((UFO) e).polygon, circle)) {
                    e.health -= 75;
                    UFODamaged = true;
                    Sound.playRandom(Sound.hit);
                    break;
                }
            } else if (e instanceof Meteor) {
                if (((Meteor) e).enabled) {
                    if (Bullet.overlaps(circle, ((Meteor) e).polygon)) {
                        e.remove();
                        Sound.playRandom(Sound.hit);
                        Stats.killsMeteor++;
                        break;
                    }
                }
            } else if (e instanceof PowerUp) {
                if (Bullet.overlaps(circle, ((PowerUp) e).polygon)) {
                    Sound.playRandom(Sound.hit);
                    e.health -= this.damage;
                    break;
                }

            }
        }
    }

    @Override
    public void render(Screen g) {
        // Код прямиком из 2000 :D Взял тут: http://willperone.net/Code/codecircle.php
        int r = (int) position;
        int xc = OuterSpace.GAME_WIDTH / 2;
        int yc = OuterSpace.GAME_HEIGHT / 2;
        int x = r, y = 0;//local coords
        int cd2 = 0;    //current distance squared - radius squared


        drawpixel(xc - r, yc, g);
        drawpixel(xc + r, yc, g);
        drawpixel(xc, yc - r, g);
        drawpixel(xc, yc + r, g);

        while (x > y)    //only formulate 1/8 of circle
        {
            cd2 -= (--x) - (++y);
            if (cd2 < 0) cd2 += x++;

            drawpixel(xc - x, yc - y, g);//upper left left
            drawpixel(xc - y, yc - x, g);//upper upper left
            drawpixel(xc + y, yc - x, g);//upper upper right
            drawpixel(xc + x, yc - y, g);//upper right right
            drawpixel(xc - x, yc + y, g);//lower left left
            drawpixel(xc - y, yc + x, g);//lower lower left
            drawpixel(xc + y, yc + x, g);//lower lower right
            drawpixel(xc + x, yc + y, g);//lower right right
        }
    }

    private void drawpixel(int x, int y, Screen g) {
        sprite.setPosition(x, y);
        sprite.draw(g.spriteBatch);
    }
}
