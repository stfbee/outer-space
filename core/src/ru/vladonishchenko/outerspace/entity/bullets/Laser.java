package ru.vladonishchenko.outerspace.entity.bullets;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.entity.Bullet;

/**
 * User: Vlad
 * Date: 09.01.14
 * Time: 22:34
 */
public class Laser extends Bullet {
    protected final double damage;
    private int tick;

    public Laser() {
        super();

        Rectangle bounds = new Rectangle(0, 0, 2, -radius);
        polygon = new Polygon(new float[]{0, 0, bounds.width, 0, bounds.width, bounds.height, 0, bounds.height});
        x = (float) (Math.cos(angle * Math.PI / 180) * position + OuterSpace.GAME_WIDTH / 2 - 1);
        y = (float) (Math.sin(angle * Math.PI / 180) * position + OuterSpace.GAME_HEIGHT / 2 - 2);
        sprite = new Sprite(Art.laser);
        sprite.setBounds(0, 0, 2, -radius);
        damage = 1f;
    }

    @Override
    public void tick() {
        super.tick();
        tick++;
        if (tick == 2) this.remove(); //40
    }
}
