package ru.vladonishchenko.outerspace.entity.bullets;

import com.badlogic.gdx.graphics.g2d.Sprite;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.entity.Bullet;

/**
 * User: Vlad
 * Date: 09.01.14
 * Time: 22:34
 */
public class Bomb extends Bullet {
    public Bomb() {
        super();
        x = (float) (Math.cos(angle * Math.PI / 180) * position + OuterSpace.GAME_WIDTH / 2 - 1);
        y = (float) (Math.sin(angle * Math.PI / 180) * position + OuterSpace.GAME_HEIGHT / 2 - 2);

        sprite = new Sprite(Art.bomb);
        damage = 1f;
    }

    @Override
    public void tick() {
        position += (2* OuterSpace.GAMESPEED);
        x = (float) (Math.cos(angle * Math.PI / 180) * position + OuterSpace.GAME_WIDTH / 2 - 1);
        y = (float) (Math.sin(angle * Math.PI / 180) * position + OuterSpace.GAME_HEIGHT / 2 - 2);
        super.tick();
    }
}
