package ru.vladonishchenko.outerspace.entity.bullets;

import com.badlogic.gdx.graphics.g2d.Sprite;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.entity.Bullet;

/**
 * User: Vlad
 * Date: 24.10.2014
 * Time: 0:00
 */
public class UFOBomb extends Bullet {
    public UFOBomb(float position, float angle) {
        super();
        this.angle = angle;
        this.position = position;
        x = (float) (Math.cos(this.angle * Math.PI / 180) * this.position + OuterSpace.GAME_WIDTH / 2 - 1);
        y = (float) (Math.sin(this.angle * Math.PI / 180) * this.position + OuterSpace.GAME_HEIGHT / 2 - 2);

        sprite = new Sprite(Art.ufobomb);
        damage = .125f;
    }

    @Override
    public void tick() {
        position -= (1.15f * OuterSpace.GAMESPEED);
        x = (float) (Math.cos(angle * Math.PI / 180) * position + OuterSpace.GAME_WIDTH / 2 - 1);
        y = (float) (Math.sin(angle * Math.PI / 180) * position + OuterSpace.GAME_HEIGHT / 2 - 2);
        super.tick();
    }

}
