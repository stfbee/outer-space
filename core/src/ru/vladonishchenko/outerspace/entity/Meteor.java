package ru.vladonishchenko.outerspace.entity;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.Sound;
import ru.vladonishchenko.outerspace.Stats;
import ru.vladonishchenko.outerspace.gui.Warning;
import ru.vladonishchenko.outerspace.screen.Screen;


/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 15:03
 */
public class Meteor extends Entity {
    public final Polygon polygon;
    private final Sprite sprite = new Sprite(Art.meteor);
    public final double damage = 0.4;
    public boolean enabled = false;
    private int tick;

    public Meteor() {
        Rectangle bounds = new Rectangle(0, 0, 28, 12);
        polygon = new Polygon(new float[]{0, 0, bounds.width, 0, bounds.width, bounds.height, 0, bounds.height});

        health = .1;
        angle = random.nextInt(360);

        level.add(new Warning(this));

        position = (float) Math.sqrt(OuterSpace.GAME_HEIGHT * OuterSpace.GAME_HEIGHT + OuterSpace.GAME_WIDTH * OuterSpace.GAME_WIDTH) / 2;
        x = (float) (Math.cos(angle * Math.PI / 180) * position + OuterSpace.GAME_WIDTH / 2 - 6);
        y = (float) (Math.sin(angle * Math.PI / 180) * position + OuterSpace.GAME_HEIGHT / 2 - 6);
        sprite.setPosition(x, y);
        sprite.setRotation(angle + 180f);
        sprite.setColor(1f, 1f, 1f, 0f);
        polygon.setPosition(x, y);
        polygon.setOrigin(14, 6);
        polygon.setRotation(angle + 180f);
    }

    public void tick() {
        tick++;
        if (tick > 300) {
            enabled = true;
            position -= (5 * OuterSpace.GAMESPEED);
            //        float deltaAngle = random.nextFloat() * 0.2f;
            //        angle -= deltaAngle;
            x = (float) (Math.cos(angle * Math.PI / 180) * position + OuterSpace.GAME_WIDTH / 2 - 14);
            y = (float) (Math.sin(angle * Math.PI / 180) * position + OuterSpace.GAME_HEIGHT / 2 - 6);

            sprite.setPosition(x, y);
            sprite.setRotation(angle + 180f);
            sprite.setColor(1f, 1f, 1f, 1f);
            polygon.setPosition(x, y);
            polygon.setOrigin(14, 6);
            polygon.setRotation(angle + 180f);

            outOfBounds();
            if (health <= 0) {
                Sound.playRandom(Sound.hit);
//                level.add(new Explosion((int)(x+14 - SpaceKilla.GAME_WIDTH / 2), (int) (y+6 - SpaceKilla.GAME_HEIGHT / 2), Explosion.Size.SMALL, (int) angle, sprite.getOriginX(), sprite.getOriginY()));
                level.add(new Explosion(x, y, Explosion.Size.SMALL, sprite.getRotation(), sprite.getOriginX(), sprite.getOriginY()));

                Stats.killsMeteor++;
                this.remove();
            }
        }
    }

    @Override
    public void render(Screen screen) {
        if (enabled) {
            sprite.draw(screen.spriteBatch);
        }
    }
}
