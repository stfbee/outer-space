package ru.vladonishchenko.outerspace.entity.powerups;

import com.badlogic.gdx.graphics.g2d.Sprite;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.entity.PowerUp;

/**
 * User: Vlad
 * Date: 08.02.14
 * Time: 0:05
 */
public class Slowly extends PowerUp {

    public Slowly() {
        super();
        sprite1 = new Sprite(Art.powerups[2][0]);
        sprite2 = new Sprite(Art.powerups[2][1]);
    }
}
