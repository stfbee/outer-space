package ru.vladonishchenko.outerspace.entity.powerups;

import com.badlogic.gdx.graphics.g2d.Sprite;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.entity.PowerUp;

/**
 * User: Vlad
 * Date: 08.02.14
 * Time: 0:05
 */
public class Laserous extends PowerUp{
    
    public Laserous() {
        super();
        sprite1 = new Sprite(Art.powerups[1][0]);
        sprite2 = new Sprite(Art.powerups[1][1]);
    }
}
