package ru.vladonishchenko.outerspace.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.Prefs;
import ru.vladonishchenko.outerspace.Sound;
import ru.vladonishchenko.outerspace.Stats;
import ru.vladonishchenko.outerspace.entity.bullets.UFOBomb;
import ru.vladonishchenko.outerspace.gui.HealthBar;
import ru.vladonishchenko.outerspace.screen.Screen;


/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 15:03
 */
public class UFO extends Entity {
    public final Circle polygon;
    public final double damage = 0.4;
    private final Sprite sprite = new Sprite(Art.ufoDisc);
    private final Sprite spritetower = new Sprite(Art.ufoTower);
    public boolean enabled = false;
    int rotation = 0;
    int progress;
    float randomShoot;
    private int tick;

    public UFO() {
//        polygon = new Polygon(new float[]{0, 0, sprite.getWidth(), 0, sprite.getWidth(), sprite.getHeight(), 0, sprite.getHeight()});
        polygon = new Circle();


        health = 100;
        angle = random.nextInt(360);

        position = (float) Math.sqrt(OuterSpace.GAME_HEIGHT * OuterSpace.GAME_HEIGHT + OuterSpace.GAME_WIDTH * OuterSpace.GAME_WIDTH) / 2;

        x = (float) (Math.cos(angle * Math.PI / 180) * position + OuterSpace.GAME_WIDTH / 2 - sprite.getWidth() / 2);
        y = (float) (Math.sin(angle * Math.PI / 180) * position + OuterSpace.GAME_HEIGHT / 2 - sprite.getHeight() / 2);
        sprite.setPosition(x, y);
        sprite.setRotation(rotation);
        sprite.setColor(1f, 1f, 1f, 0f);
        spritetower.setPosition(x, y);
        spritetower.setRotation(angle);
        spritetower.setColor(1f, 1f, 1f, 0f);
        polygon.setRadius(16);
        polygon.setPosition((x + 16) * OuterSpace.SCREEN_SCALE, (y + 16) * OuterSpace.SCREEN_SCALE);
//        polygon.setOrigin(sprite.getWidth() / 2, sprite.getHeight() / 2);
//        polygon.setRotation(rotation);

        randomShoot = (75f + (random.nextFloat() * 50));
        progress = (270 + random.nextInt(400)) * 2;
        level.add(new HealthBar(this));
    }

    public void tick() {
        tick++;
        randomShoot -= OuterSpace.GAMESPEED;
        enabled = true;
        progress--;
        rotation = rotation >= 360 ? 0 : rotation + 2;
        angle = angle >= 360 ? 0 : angle - 0.5f;
        if (progress > 0) {
            position -= (.75 * OuterSpace.GAMESPEED);
            if (position <= 60) position = 60;
        } else {
            position += (.75 * OuterSpace.GAMESPEED);
        }
        x = (float) (Math.cos(angle * Math.PI / 180) * position + OuterSpace.GAME_WIDTH / 2 - sprite.getWidth() / 2);
        y = (float) (Math.sin(angle * Math.PI / 180) * position + OuterSpace.GAME_HEIGHT / 2 - sprite.getHeight() / 2);

        sprite.setPosition(x, y);
        sprite.setRotation(rotation);
        sprite.setColor(1f, 1f, 1f, 1f);
        spritetower.setPosition(x, y);
        spritetower.setRotation(angle);
        spritetower.setColor(1f, 1f, 1f, 1f);
        polygon.setPosition((x + 16), (y + 16));
//        polygon.setOrigin(sprite.getWidth() / 2, sprite.getHeight() / 2);
//        polygon.setRotation(rotation);

        if (level != null) {
            for (Entity e : level.entities) {
                if (e instanceof Meteor) {
                    if (Bullet.overlaps(this.polygon, ((Meteor) e).polygon)) {
                        health -= (((Meteor) e).damage)*10;
                        ((Meteor) e).health = 0;
                        Sound.playRandom(Sound.hit);
                        if (Prefs.isVibro()) Gdx.input.vibrate(50);
                        Stats.damageMeteor++;
                    }
                }
            }
        }

        if (health <= 0) {
            Sound.playRandom(Sound.explosion);
            level.add(new Explosion(x, y, Explosion.Size.BIG, sprite.getRotation(), sprite.getOriginX(), sprite.getOriginY()));
            Stats.killsUFO++;
            this.remove();
        }


        if (randomShoot <= 0) {
            level.add(new UFOBomb(position, angle));
            Sound.playRandom(Sound.shot);
            randomShoot = (75f + (random.nextFloat() * 50));
        }

        outOfBounds();
    }

    @Override
    public void render(Screen screen) {
        if (enabled) {
            sprite.draw(screen.spriteBatch);
            spritetower.draw(screen.spriteBatch);
        }
    }
}
