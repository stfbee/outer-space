package ru.vladonishchenko.outerspace.entity;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;

import ru.vladonishchenko.outerspace.Art;
import ru.vladonishchenko.outerspace.OuterSpace;
import ru.vladonishchenko.outerspace.Sound;
import ru.vladonishchenko.outerspace.Stats;
import ru.vladonishchenko.outerspace.gui.HealthBar;
import ru.vladonishchenko.outerspace.screen.Screen;

/**
 * User: vladik
 * Date: 05.11.13
 * Time: 12:17
 */
/* 14x22 */
public class Worm extends Entity {
    public final Polygon polygon;
    public double damage = 0.1;
    public boolean stopped = false;
    private Sprite sprite = new Sprite(Art.worm[0][0]);
    private Sprite sprite2 = new Sprite(Art.teeth[0][0]);
    private int frame;
    private long tick;
    private Color color;

    private Color colors[] = new Color[]{
            new Color(114 / 255f, 122 / 255f, 79 / 255f, 1f),
            new Color(131 / 255f, 70 / 255f, 70 / 255f, 1f),
            new Color(178 / 255f, 0 / 255f, 0 / 255f, 1f),
            new Color(62 / 255f, 138 / 255f, 138 / 255f, 1f),
            new Color(168 / 255f, 124 / 255f, 32 / 255f, 1f),
            new Color(176 / 255f, 25 / 255f, 115 / 255f, 1f),
    };
    private int index = random.nextInt(colors.length);

    public Worm() {
        Rectangle bounds = new Rectangle(0, 0, 14, 22);
        polygon = new Polygon(new float[]{0, 0, bounds.width, 0, bounds.width, bounds.height, 0, bounds.height});
        angle = random.nextInt(360);
        position = (float) Math.sqrt((OuterSpace.GAME_HEIGHT / 2 * OuterSpace.GAME_HEIGHT / 2) + (OuterSpace.GAME_WIDTH / 2 * OuterSpace.GAME_WIDTH / 2));
        x = (float) (Math.cos(angle * Math.PI / 180) * position + OuterSpace.GAME_WIDTH / 2);
        y = (float) (Math.sin(angle * Math.PI / 180) * position + OuterSpace.GAME_HEIGHT / 2);
        sprite.setPosition(x - sprite.getOriginX(), y - sprite.getOriginY());
        sprite.setRotation(angle + 90f);
        sprite2.setPosition(x - sprite.getOriginX(), y - sprite.getOriginY());
        sprite2.setRotation(angle + 90f);
        color = randomColor();

        health = 3;
        level.add(new HealthBar(this));
    }

    public void tick() {
        tick++;
        float frameRate = stopped ? 10f : 5f;
        if (tick % (int) (frameRate / OuterSpace.GAMESPEED) == 0) {
            frame++;
            frame = frame % 4;
        }
        if (stopped) {
            damage = (tick % (int) (40f / OuterSpace.GAMESPEED) == 0) ? 0.1 : 0;
        } else {
            position -= (.125 * OuterSpace.GAMESPEED);
        }
        x = (float) (Math.cos(angle * Math.PI / 180) * position + OuterSpace.GAME_WIDTH / 2 - sprite.getOriginX());
        y = (float) (Math.sin(angle * Math.PI / 180) * position + OuterSpace.GAME_HEIGHT / 2 - sprite.getOriginY());


        sprite.setRegion(Art.worm[frame][0]);
        sprite.setPosition(x, y);
        sprite.setColor(color);
        sprite2.setRegion(Art.teeth[frame][0]);
        sprite2.setPosition(x, y);

        polygon.setOrigin(sprite.getOriginX(), sprite.getOriginY());
        polygon.setPosition(sprite.getX(), sprite.getY());
        polygon.setRotation(sprite.getRotation());
//        if (tick % 10 == 0) {
//            health += .1;
//            if (health > 1) health = 0;
//        }

        if (health <= 0) {
            Sound.playRandom(Sound.explosion);
            level.add(new Explosion(x, y, Explosion.Size.WORM, sprite.getRotation(), sprite.getOriginX(), sprite.getOriginY(), color));
            Stats.killsWorm++;

//            final String MY_ACHIEVEMEMENT_ID = "...."; // your achievement ID here
//
//
//            if (OuterSpace.mGoogleApiClient != null && OuterSpace.mGoogleApiClient.isConnected()) {
//                // Call a Play Games services API method, for example:
//                Achievements.unlock(OuterSpace.mGoogleApiClient, MY_ACHIEVEMENT_ID);
//            } else {
//                // Alternative implementation (or warn user that they must
//                // sign in to use this feature)
//            }


            this.remove();
        }
    }

    @Override
    public void render(Screen screen) {
        sprite.draw(screen.spriteBatch);
        sprite2.draw(screen.spriteBatch);
    }

    private Color randomColor() {
        float r, g, b;

        Color _color = new Color(colors[index]);

        do {
            r = random.nextFloat();
        } while (r > .3f);
        do {
            g = random.nextFloat();
        } while (g > .3f);
        do {
            b = random.nextFloat();
        } while (b > .3f);

        _color.r += (r - .15f);
        _color.g += (g - .15f);
        _color.b += (b - .15f);

        return _color;
    }
}
