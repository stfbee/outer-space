package ru.vladonishchenko.outerspace;

/**
 * User: Vlad
 * Date: 15.02.14
 * Time: 22:24
 */
public interface ActionResolver {
    public boolean getSignedInGPGS();
    public void loginGPGS();
    public void submitScoreGPGS(int score);
    public void unlockAchievementGPGS(String achievementId);
    public void getLeaderboardGPGS();
    public void getAchievementsGPGS();
}