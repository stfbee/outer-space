package ru.vladonishchenko.outerspace;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import ru.vladonishchenko.outerspace.screen.Screen;
import ru.vladonishchenko.outerspace.screen.screens.GameScreen;
import ru.vladonishchenko.outerspace.screen.screens.TitleScreen;


/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 1:44
 */
public class OuterSpace extends ApplicationAdapter {
    public static float SCREEN_SCALE = 4;
    public static int GAME_WIDTH = 320;
    public static int GAME_HEIGHT = 180;
    public static boolean running = false;
    public static boolean DEBUG = true;
    public static boolean DEBUG_MONSTERS = false;
    public static boolean GAMEOVER = false;
    public static boolean DEMOMODE;
    private final Input input = new Input();
    private Screen screen;
    private float accum = 0;
    public static float GAMESPEED = 1f;

    public static BitmapFont font8;
    public static BitmapFont font16;
    public static BitmapFont font32;
    public static BitmapFont fontDemo;

    public static String nedeedLetters = "ЁёйцукенгшщзхъфывапролджэячсмитьбюЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮQWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890!-_:<>@#$%^&*=+,.?() ";
    public static IGoogleServices googleServices;

    public OuterSpace(IGoogleServices googleServices, boolean dm) {
        OuterSpace.googleServices = googleServices;
        DEMOMODE = dm;
    }

    @Override
    public void create() {
        Art.load();
        Sound.load();
        Prefs.load();
        L.load();
        loadFonts();

        Gdx.input.setInputProcessor(input);
        running = true;
        setScreen((!OuterSpace.DEBUG_MONSTERS) ? new TitleScreen() : new GameScreen());
    }

    @Override
    public void pause() {
        running = false;
    }

    @Override
    public void resume() {
        running = true;
        loadFonts();
    }

    private void loadFonts() {
        FileHandle fontFile = Gdx.files.internal("res/spacekilla.ttf");
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(fontFile);
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.characters = nedeedLetters;
        parameter.flip = true;
        parameter.size = 8;
        font8 = generator.generateFont(parameter);
        fontDemo = generator.generateFont(parameter);
        parameter.size = 16;
        font16 = generator.generateFont(parameter);
        font32 = generator.generateFont(parameter);
        fontDemo.setColor(Color.RED);
        generator.dispose();
    }

    public void setScreen(Screen newScreen) {
        if (screen != null) screen.removed();
        screen = newScreen;
        if (screen != null) screen.init(this);
    }

    @Override
    public void render() {
//        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        accum += Gdx.graphics.getDeltaTime();
        while (accum > 1.0f / 60.0f) {
            screen.tick(input);
            input.tick();
            accum -= 1.0f / 60.0f;
        }
        screen.render();
    }
}
