package ru.vladonishchenko.outerspace;

/**
 * Created by Onishchenko Vladislav
 * Date: 24.09.2014
 * Time: 13:05
 */
public class DemoTimer {
    private static int limit = 60 * 5;
    private static int time = limit;
    private static long tick = 0;

    public static void restartTimer() {
        time = limit;
    }

    public static void tick() {
        tick++;
        if (tick % 60 == 0) {
            time--;
        }
    }

    public static String getTime() {
        String seconds = String.valueOf(time % 60);
        String minutes = String.valueOf(time / 60);
        if (minutes.length() == 1) minutes = "0" + minutes;
        if (seconds.length() == 1) seconds = "0" + seconds;

        return minutes + ":" + seconds;
    }

    public static boolean isFinished() {
        return (time <= 0);
    }
}
