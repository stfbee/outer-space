package ru.vladonishchenko.outerspace;

/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 1:59
 */

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

@SuppressWarnings({"WeakerAccess", "SameParameterValue"})
public class Art {
    public static TextureRegion bg;
    public static TextureRegion stars1;
    public static TextureRegion stars2;
    public static TextureRegion stars3;
    public static TextureRegion warning2;
    public static TextureRegion stfbee;


    /*bullets*/
    public static TextureRegion bomb;
    public static TextureRegion ufobomb;
    public static TextureRegion laser;
    public static TextureRegion flash;
    public static TextureRegion flashPixel;


    /*entities*/
    public static TextureRegion[][] worm;
    public static TextureRegion[][] teeth;
    public static TextureRegion[][] explosion;
    public static TextureRegion[][] powerups;
    public static TextureRegion meteor;
    public static TextureRegion player;
    public static TextureRegion player_right;
    public static TextureRegion player_left;
    public static TextureRegion ufoDisc;
    public static TextureRegion ufoTower;
    public static TextureRegion[][] sparkle;


    /*bars*/
    public static TextureRegion healthbar_big;
    public static TextureRegion healthstep_big;
    public static TextureRegion healthbar_big_red;
    public static TextureRegion healthstep_big_red;
    public static TextureRegion healthbar_small;
    public static TextureRegion healthbar_ufo;
    public static TextureRegion healthstep_small;
    public static TextureRegion chargebar_big;
    public static TextureRegion chargestep_big;
    public static TextureRegion chargebar_big_over;
    public static TextureRegion healthbar_big_over;
    public static TextureRegion[][] healthbar_worm;
    public static TextureRegion[][] healthbar_ufosteps;


    /*buttons*/
    public static NinePatch button;

    public static void load() {
        bg = load("res/gui/background.png", 320, 240);
        stars1 = load("res/gui/stars1.png", 320, 240);
        stars2 = load("res/gui/stars2.png", 320, 240);
        stars3 = load("res/gui/stars3.png", 320, 240);
        stfbee = load("res/stfbee.png", 106, 76);
        warning2 = load("res/gui/warning2.png", 29, 64);


        /*bars*/
        healthbar_small = load("res/gui/bars/healthbar_small.png", 16, 3);
        healthbar_ufo = load("res/gui/bars/healthbar_ufo.png", 41, 3);
        healthbar_big = load("res/gui/bars/healthbar_big.png", 44, 16);
        healthbar_big_red = load("res/gui/bars/healthbar_big_red.png", 44, 16);
        chargebar_big = load("res/gui/bars/chargebar_big.png", 44, 16);
        healthstep_small = load("res/gui/bars/healthstep_small.png", 1, 1);
        healthstep_big = load("res/gui/bars/healthstep_big.png", 5, 10);
        healthstep_big_red = load("res/gui/bars/healthstep_big_red.png", 5, 10);
        chargestep_big = load("res/gui/bars/chargestep_big.png", 5, 10);
        chargebar_big_over = load("res/gui/bars/chargebar_big_over.png", 52, 24);
        healthbar_big_over = load("res/gui/bars/healthbar_big_over.png", 52, 24);
        healthbar_worm = split("res/gui/bars/healthbar_worm.png", 14, 1);
        healthbar_ufosteps = split("res/gui/bars/healthbar_ufosteps.png", 39, 1);


        /*entities*/
        worm = split("res/entities/worm_gray.png", 14, 22);
        teeth = split("res/entities/teeth.png", 14, 22);
        meteor = load("res/entities/meteor.png", 28, 12);
        powerups = split("res/entities/powerups.png", 6, 9);
        explosion = split("res/entities/explosion.png", 32, 32);
        player = load("res/entities/player.png", 14, 18);
        player_left = load("res/entities/player_left.png", 14, 18);
        player_right = load("res/entities/player_right.png", 14, 18);
        ufoDisc = load("res/entities/ufodisc.png", 32, 32);
        ufoTower = load("res/entities/ufotower.png", 32, 32);
        sparkle = split("res/entities/sparkle.png", 9, 15);


        /*bullets*/
        bomb = load("res/entities/bomb.png", 2, 4);
        ufobomb = load("res/entities/ufobomb.png", 2, 4);
        laser = load("res/entities/laser.png", 1, 5);
        flash = load("res/entities/flash.png", 1, 5);
        flashPixel = load("res/entities/flashPixel.png", 1, 1);


        /*buttons*/
        button = loadNP("res/gui/buttons/button.9.png", 4, 4, 4, 4);
    }

    private static NinePatch loadNP(String name, int left, int right, int top, int bottom) {
        return new NinePatch(new TextureRegion(new Texture(Gdx.files.internal(name)), 11, 11), left, right, top, bottom);
    }

    private static TextureRegion[][] split(String name, int width, int height) {
        return split(name, width, height, false);
    }

    private static TextureRegion[][] split(String name, int width, int height, boolean flipX) {
        Texture texture = new Texture(Gdx.files.internal(name));
        int xSlices = texture.getWidth() / width;
        int ySlices = texture.getHeight() / height;
        TextureRegion[][] res = new TextureRegion[xSlices][ySlices];
        for (int x = 0; x < xSlices; x++) {
            for (int y = 0; y < ySlices; y++) {
                res[x][y] = new TextureRegion(texture, x * width, y * height, width, height);
                res[x][y].flip(flipX, true);
            }
        }
        return res;
    }

    private static TextureRegion load(String name, int width, int height) {
        Texture texture = new Texture(Gdx.files.internal(name));
        TextureRegion region = new TextureRegion(texture, 0, 0, width, height);
        region.flip(false, true);
        return region;
    }
}