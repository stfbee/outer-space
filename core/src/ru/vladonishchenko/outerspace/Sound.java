package ru.vladonishchenko.outerspace;

/**
 * User: Vlad
 * Date: 28.10.13
 * Time: 1:59
 */

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Sound {

    public static final com.badlogic.gdx.audio.Sound[] explosion = new com.badlogic.gdx.audio.Sound[3];
    public static final com.badlogic.gdx.audio.Sound[] flash = new com.badlogic.gdx.audio.Sound[2];
    public static final com.badlogic.gdx.audio.Sound[] hit = new com.badlogic.gdx.audio.Sound[2];
    public static final com.badlogic.gdx.audio.Sound[] laser = new com.badlogic.gdx.audio.Sound[3];
    public static final com.badlogic.gdx.audio.Sound[] powerup = new com.badlogic.gdx.audio.Sound[2];
    public static final com.badlogic.gdx.audio.Sound[] shot = new com.badlogic.gdx.audio.Sound[5];
    private static final HashMap<Long, com.badlogic.gdx.audio.Sound> soundID = new HashMap<>();
    public static final String menuMusicPath = "res/sound/menu.mp3";
    public static final String gameMusicPath = "res/sound/game.mp3";
    private static Music music;
    private static float musicVolume = .3f;
    private static int musicPlaying = 0;

    public static void load() {
        explosion[1] = load("res/sound/explosion1.wav");
        explosion[2] = load("res/sound/explosion2.wav");
        explosion[0] = load("res/sound/explosion3.wav");
        laser[1] = load("res/sound/laser1.wav");
        laser[2] = load("res/sound/laser2.wav");
        laser[0] = load("res/sound/laser3.wav");
        flash[1] = load("res/sound/flash1.wav");
        flash[0] = load("res/sound/flash2.wav");
        hit[1] = load("res/sound/hit1.wav");
        hit[0] = load("res/sound/hit2.wav");
        powerup[1] = load("res/sound/powerup1.wav");
        powerup[0] = load("res/sound/powerup2.wav");
        shot[1] = load("res/sound/shot1.wav");
        shot[2] = load("res/sound/shot2.wav");
        shot[3] = load("res/sound/shot3.wav");
        shot[4] = load("res/sound/shot4.wav");
        shot[0] = load("res/sound/shot5.wav");
    }

    public static void playRandom(com.badlogic.gdx.audio.Sound[] s) {
        playRandom(s, 1);
    }

    public static void playRandom(com.badlogic.gdx.audio.Sound[] s, float volume) {
        if (Prefs.isSound()) {
            com.badlogic.gdx.audio.Sound sound = s[new Random().nextInt(s.length)];
            long id = sound.play();
            Sound.soundID.put(id, sound);
            float deltaPitch = new Random().nextFloat() / 5 - .1f;

            sound.setPitch(id, OuterSpace.GAMESPEED * (1 + deltaPitch));
            sound.setVolume(id, volume);
        }
    }

    public static void setSpeed(float speed) {
        if (Prefs.isSound()) {
            for (Map.Entry<Long, com.badlogic.gdx.audio.Sound> entry : soundID.entrySet()) {
                Long id = entry.getKey();
                com.badlogic.gdx.audio.Sound sound = entry.getValue();
                sound.setPitch(id, speed);
            }
        }
    }

    public static void playMenuMusic(){
        if (Prefs.isSound()) {
            if(musicPlaying == 1) return;
            if(music != null) music.dispose();
            musicPlaying = 1;
            music = Gdx.audio.newMusic(Gdx.files.internal(menuMusicPath));
            music.setVolume(musicVolume);
            music.setLooping(true);
            music.play();
        }
    }
    public static void playGameMusic(){
        if (Prefs.isSound()) {
            if(musicPlaying == 2) return;
            if(music != null) music.dispose();
            musicPlaying = 2;
            music = Gdx.audio.newMusic(Gdx.files.internal(gameMusicPath));
            music.setVolume(musicVolume);
            music.setLooping(true);
            music.play();
        }
    }
    public static void stopMusic(){
        musicPlaying = 0;
        music.stop();
        music.dispose();
    }

    private static com.badlogic.gdx.audio.Sound load(String name) {
        return Gdx.audio.newSound(Gdx.files.internal(name));
    }



}