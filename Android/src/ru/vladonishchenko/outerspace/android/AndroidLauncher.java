package ru.vladonishchenko.outerspace.android;

import android.os.Bundle;
import android.view.WindowManager;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;

import ru.vladonishchenko.outerspace.DesktopGoogleServices;
import ru.vladonishchenko.outerspace.OuterSpace;

public class AndroidLauncher extends AndroidApplication {
    private StartAppAd startAppAd = new StartAppAd(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        StartAppSDK.init(this, "111462516", "211148316", true);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        initialize(new OuterSpace(new DesktopGoogleServices(),BuildConfig.FLAVOR.equals("demo")), config);
    }

    @Override
    public void onResume() {
        super.onResume();
        startAppAd.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        startAppAd.onPause();
    }


    @Override
    public void onBackPressed() {
        startAppAd.onBackPressed();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        startAppAd.onBackPressed();
        super.onDestroy();
    }
}
