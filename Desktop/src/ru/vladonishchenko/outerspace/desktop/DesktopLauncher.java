package ru.vladonishchenko.outerspace.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import ru.vladonishchenko.outerspace.DesktopGoogleServices;
import ru.vladonishchenko.outerspace.OuterSpace;

public class DesktopLauncher {
    public static void main(String[] arg) {
        boolean dm = false;

        if (arg.length > 1) {
            if (arg[0].equals("demo")) {
                dm = true;
            }
        }

        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "Outer Space";
        cfg.useGL30 = true;
        cfg.resizable = true;
        cfg.width = 1280;
        cfg.height = 720;

        new LwjglApplication(new OuterSpace(new DesktopGoogleServices(), dm), cfg);
    }
}
